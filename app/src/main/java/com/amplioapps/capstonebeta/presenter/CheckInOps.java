package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;

import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.model.auth.AccountConfig;
import com.amplioapps.capstonebeta.view.CheckInActivity;

import java.lang.ref.WeakReference;
import java.util.Date;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;


public class CheckInOps implements ConfigurableOps, ContextualOps {

    protected final static String TAG =
            LoginOps.class.getSimpleName();

    protected WeakReference<CheckInActivity> mActivity;

    private AccountManager mAccountManager;
    private Account mAccount;

    private CheckInCommand mCheckInCommand;

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((CheckInActivity) activity);

        if (firstTimeIn){
            mCheckInCommand = new CheckInCommand(this);
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();
        }
    }

    public void cancelCheckIn() {
        getActivity().setResult(getActivity().RESULT_CANCELED);
        getActivity().finish();
    }

    public void runCheckIn(String... params){
        String bloodSugar = params[0];
        String insulin = params[1];
        String meal = params[2];

        long ownerId = Long.parseLong(
                mAccountManager.getUserData(
                        mAccount,
                        AccountConfig.USERDATA_USER_OBJ_ID));

        CheckInItem checkIn = new CheckInItem();
        checkIn.setTimestamp(new Date().getTime());
        checkIn.setOwnerId(ownerId);
        checkIn.addAnswer(1L, bloodSugar);
        checkIn.addAnswer(2L, insulin);
        checkIn.addAnswer(3L, meal);

        mCheckInCommand.execute(checkIn);
    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public Activity getActivity(){
        return mActivity.get();
    }

    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }

}
