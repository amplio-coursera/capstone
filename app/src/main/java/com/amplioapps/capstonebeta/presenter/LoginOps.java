package com.amplioapps.capstonebeta.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.amplioapps.capstonebeta.commons.AuthenticatorOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.view.LoginActivity;
import com.amplioapps.capstonebeta.view.SignupActivity;

import java.lang.ref.WeakReference;


public class LoginOps implements ContextualOps, AuthenticatorOps {

    private final String TAG = this.getClass().getSimpleName();

    protected WeakReference<LoginActivity> mActivity;

    private LoginCommand mLoginCommand;
    private FinishLoginCommand mFinishLoginCommand;

    public void onConfiguration(Activity activity,
                         boolean firstTimeIn) {

        mActivity = new WeakReference<>((LoginActivity) activity);

        if (firstTimeIn){
            mLoginCommand = new LoginCommand(this);
            mFinishLoginCommand = new FinishLoginCommand(this);
        }
    }

    public void onUserLogin(String... params){
        // Execute Command Object
        mLoginCommand.execute(params);
    }

    public void onUserRegister(){
        Intent signup = new Intent(
                getActivity().getBaseContext(), SignupActivity.class);

        if (getActivity().getIntent().getExtras() != null)
            signup.putExtras(getActivity().getIntent().getExtras());

        mActivity.get().startSignupActivity(signup);
    }

    public void onSignupResult(Intent data){
        mFinishLoginCommand.execute(data);
    }

    public Activity getActivity(){
        return mActivity.get();
    }

    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }

}
