package com.amplioapps.capstonebeta.presenter;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.GenericAsyncTask;
import com.amplioapps.capstonebeta.commons.GenericAsyncTaskOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.auth.SignupAccountDetails;

import static com.amplioapps.capstonebeta.model.auth.WebSvcConfig.sWebSvcAuth;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.PARAM_USER_PASS;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.KEY_ERROR_MESSAGE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.USERDATA_USER_OBJ_ID;


public class SignupCommand extends GenericAsyncTaskOps<SignupAccountDetails, Void, Intent>
        implements Command<SignupAccountDetails> {

    private final String TAG = this.getClass().getSimpleName();

    private String mAccountType;

    private SignupOps mOps;

    public SignupCommand(SignupOps signupOps) {
        mOps = signupOps;
    }

    @Override
    public void execute(SignupAccountDetails details) {
        mAccountType = mOps.getActivity().getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        final GenericAsyncTask<SignupAccountDetails,
                Void,
                Intent,
                SignupCommand> asyncTask =
                new GenericAsyncTask<>(this);

        // Execute the GenericAsyncTask.
        asyncTask.execute(details);
    }

    /**
     * Run in a background Thread to avoid blocking the UI Thread.
     */
    @SuppressWarnings("unchecked")
    @Override
    public Intent doInBackground(SignupAccountDetails... accounts) {
        Log.d(TAG, "Started authenticating");

        SignupAccountDetails details  = accounts[0];

        String authtoken = null;
        Bundle data = new Bundle();
        try {
            authtoken = sWebSvcAuth.signUp(details);

            data.putString(AccountManager.KEY_ACCOUNT_NAME, details.getEmail());
            data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType);
            data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
            data.putString(PARAM_USER_PASS, details.getPassword());

            Bundle userData = new Bundle();
            userData.putString(USERDATA_USER_OBJ_ID, String.valueOf(sWebSvcAuth.getUserId()));
            data.putBundle(AccountManager.KEY_USERDATA, userData);

        } catch (Exception e) {
            data.putString(KEY_ERROR_MESSAGE, e.getMessage());
        }

        final Intent res = new Intent();
        res.putExtras(data);
        return res;
    }

    /**
     * A count of the number of results in the query are displayed in
     * the UI Thread.
     */
    @Override
    public void onPostExecute(Intent intent) {

        if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
            Utils.showToast(mOps.getActivity().getApplicationContext(),
                    intent.getStringExtra(KEY_ERROR_MESSAGE));
        } else {
            mOps.getActivity().setResult(mOps.getActivity().RESULT_OK, intent);
            mOps.getActivity().finish();
        }
    }

}
