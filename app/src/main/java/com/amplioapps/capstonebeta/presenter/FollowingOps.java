package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.SimpleUser;
import com.amplioapps.capstonebeta.model.UserListAdapter;
import com.amplioapps.capstonebeta.model.auth.AccountConfig;
import com.amplioapps.capstonebeta.view.FollowerActivity;
import com.amplioapps.capstonebeta.view.FollowingActivity;

import java.lang.ref.WeakReference;
import java.util.Collection;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;

public class FollowingOps implements ConfigurableOps, ContextualOps {
    protected final String TAG =
            this.getClass().getSimpleName();

    protected WeakReference<FollowingActivity> mActivity;

    protected AccountManager mAccountManager;
    protected Account mAccount;

    private UserListAdapter mCursorAdapter;
    private Cursor mCursor;
    private ContentResolver cr;

    protected final ContentObserver followerChangeContentObserver =
            new ContentObserver(new Handler()) {
                /**
                 * Trigger a query and display the results.
                 */
                @Override
                public void onChange (boolean selfChange) {
                    queryFollowers();
                }
            };

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((FollowingActivity) activity);

        if (firstTimeIn){
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();

            // Initialize the SimpleCursorAdapter.
            mCursorAdapter =
                    new UserListAdapter(
                            UserListAdapter.FOLLOWING_LIST_TYPE,
                            getApplicationContext(),
                            R.layout.user_item,
                            null,
                            CheckInContract.sFollowerUserColumnsToDisplay,
                            CheckInContract.sFollowerUserColumnResIds,
                            1);

            cr = getApplicationContext().getContentResolver();

            // Unregister the ContentObserver.
            unregisterContentObserver();

            // Register the ContentObserver.
            registerContentObserver();
        } else if (mCursor != null)
            // Redisplay the contents of the cursor after a runtime
            // configuration change.
            displayCursor(mCursor);

    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public void showFollowers(){
        displayCursor(queryFollowers());
    }


    private Cursor queryFollowers(){

        String userid = mAccountManager.getUserData(
                mAccount,
                AccountConfig.USERDATA_USER_OBJ_ID);


        return cr.query(CheckInContract.FOLLOWING_USER_URI,
                null,
                null,
                new String[] { userid },
                null);
    }

    /**
     * Register the ContentObserver.
     */
    protected void registerContentObserver() {
        // Register a ContentObserver that's notified when Contacts
        // change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().registerContentObserver
                (CheckInContract.CheckInEntry.CONTENT_URI,
                        true,
                        followerChangeContentObserver);
    }

    /**
     * Unregister the ContentObserver.
     */
    protected void unregisterContentObserver() {
        // Unregister a ContentObserver so it won't be notified when
        // Contacts change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().unregisterContentObserver
                (followerChangeContentObserver);
    }

    public SimpleCursorAdapter makeCursorAdapter() {
        return mCursorAdapter;
    }

    /**
     * Display the contents of the cursor as a ListView.
     */
    public void displayCursor(Cursor cursor) {
        // Store the most recent result from a query so the display
        // can be updated after a runtime configuration change.
        mCursor = cursor;

        // Display the designated columns in the cursor as a List in
        // the ListView connected to the SimpleCursorAdapter.
        mCursorAdapter.changeCursor(cursor);
    }


    public Activity getActivity(){
        return mActivity.get();
    }


    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }
}
