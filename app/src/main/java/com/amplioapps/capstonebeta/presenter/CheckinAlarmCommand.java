package com.amplioapps.capstonebeta.presenter;


import android.accounts.Account;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.ContextualOps;

public class CheckinAlarmCommand implements Command<ContextualOps>{

    // Variables
    private ContextualOps mOps;

    // Variables for Alarm and Broadcast Receiver
    private AlarmManager mAlarmManager;
    private Intent mCheckinAlarmNotifRecIntent;
    private PendingIntent mCheckinAlarmNotifRecPendingIntent;
    private long mInitialAlarmDelay;


    // Constructor
    public CheckinAlarmCommand(){

    }

    // Set time method
    public void setInitialDelay(long delay){
        mInitialAlarmDelay = delay;
    }

    @Override
    public void execute(ContextualOps ops) {

        //  Set Ops Object
        mOps = ops;

        // ALARM LOGIC
        // Run on the UI Thread
        // Get Alarm Service
        mAlarmManager = (AlarmManager) mOps.getActivity().getSystemService(Context.ALARM_SERVICE);

        // Create Intent to Broadcast
        mCheckinAlarmNotifRecIntent = new Intent(mOps.getApplicationContext(), CheckinAlarmNotificationReceiver.class);

        // Create Pending Intent
        mCheckinAlarmNotifRecPendingIntent = PendingIntent.getBroadcast(mOps.getApplicationContext(), 0,
                mCheckinAlarmNotifRecIntent, 0);

        // Set Repeating Alarm
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + mInitialAlarmDelay,
                2*60*1000,
                mCheckinAlarmNotifRecPendingIntent);


    }


}
