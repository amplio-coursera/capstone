package com.amplioapps.capstonebeta.presenter;

import android.content.Intent;
import android.widget.Toast;
import android.provider.MediaStore;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.GenericAsyncTaskOps;

public class TakePictureCommand extends GenericAsyncTaskOps<String, Void, Intent>
        implements Command<String[]> {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    SignupOps mSignUpOps;

    public TakePictureCommand(SignupOps signupOps) {
        mSignUpOps = signupOps;
    }

    @Override
    public Intent doInBackground(String... params) {
        return null;
    }

    @Override
    public void execute(String... params) {
        dispatchTakePictureIntent();
    }

    @Override
    public void onPostExecute(Intent intent) {

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                mSignUpOps.getActivity().getPackageManager()) != null) {

            mSignUpOps.getActivity().startActivityForResult(
                    takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
}
