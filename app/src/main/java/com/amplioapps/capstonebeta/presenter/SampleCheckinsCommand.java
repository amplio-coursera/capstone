package com.amplioapps.capstonebeta.presenter;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.CheckInItem;

import java.util.ArrayList;
import java.util.Random;


public class SampleCheckinsCommand implements Command<ChartOps> {

    // VARIABLES
    private ChartOps mOps;

    // Constructor
    public SampleCheckinsCommand() {

    }

    @Override
    public void execute(ChartOps param) {

        // Assign ops object
        mOps = param;

        // Create 10 Samples and set the ArrayList
        mOps.setCheckIns(createSample(10));
    }

    // Create Sample Data
    private ArrayList<CheckInItem> createSample(int count) {

        int sampleSize = count;
        ArrayList<CheckInItem> checkInItems = new ArrayList<>();

        for (int i = 0; i < sampleSize; i++) {

            long lookBack = (long) i * 1 * 30 * 1000; // 30 Seconds times the count
            Random random = new Random();

            // Random Answers
            String randomBloodSugar = Integer.toString(random.nextInt(((300 - 20) + 1) + 20));
            String randomInsulin = Integer.toString(random.nextInt(((500 - 0) + 1) + 0));

            /*
            Current question ID / Answer pair
            checkIn.addAnswer(1L, bloodSugar);
            checkIn.addAnswer(2L, insulin);
            checkIn.addAnswer(3L, meal);
             */
            CheckInItem checkInItem = new CheckInItem();
            checkInItem.setTimestamp(mOps.getCurrentTime() - (lookBack));
            checkInItem.addAnswer(1L, randomBloodSugar);
            checkInItem.addAnswer(2L, randomInsulin);

            // Add new CheckInItem to the front of the ArrayList
            if (!checkInItems.contains(checkInItem)) {
                checkInItems.add(checkInItems.size(), checkInItem);
            }

        }
        return checkInItems;
    }
}
