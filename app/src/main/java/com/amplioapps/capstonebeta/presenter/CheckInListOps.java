package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.widget.SimpleCursorAdapter;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInListAdapter;
import com.amplioapps.capstonebeta.model.auth.AccountConfig;
import com.amplioapps.capstonebeta.view.CheckInActivity;
import com.amplioapps.capstonebeta.view.CheckInListActivity;
import com.amplioapps.capstonebeta.view.SignupActivity;

import java.lang.ref.WeakReference;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;

public class CheckInListOps implements ConfigurableOps, ContextualOps {

    protected final String TAG =
            this.getClass().getSimpleName();

    protected WeakReference<CheckInListActivity> mActivity;

    protected CheckInListAdapter mCursorAdapter;

    private AccountManager mAccountManager;

    private Account mAccount;

    protected Cursor mCursor;

    private ContentResolver cr;

    private String mUserid;

    protected final ContentObserver checkinChangeContentObserver =
            new ContentObserver(new Handler()) {
                /**
                 * Trigger a query and display the results.
                 */
                @Override
                public void onChange (boolean selfChange) {
                    queryCheckIns();
                }
            };

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((CheckInListActivity) activity);

        if (firstTimeIn){
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();

            mUserid = mAccountManager.getUserData(
                    mAccount,
                    AccountConfig.USERDATA_USER_OBJ_ID);

            // Initialize the SimpleCursorAdapter.
            mCursorAdapter =
                    new CheckInListAdapter(getApplicationContext(),
                            R.layout.checkin_item,
                            null,
                            CheckInContract.CheckInEntry.sColumnsToDisplay,
                            CheckInContract.CheckInEntry.sColumnResIds,
                            1);

            cr = getApplicationContext().getContentResolver();

            // Unregister the ContentObserver.
            unregisterContentObserver();

            // Register the ContentObserver.
            registerContentObserver();
        } else if (mCursor != null)
            // Redisplay the contents of the cursor after a runtime
            // configuration change.
            displayCursor(mCursor);

    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public void showCheckins(){
        displayCursor(queryCheckIns());
    }

    private Cursor queryCheckIns(){

        return cr.query(CheckInContract.CHECKIN_USER_URI,
                null,
                null,
                new String[] { mUserid },
                null);
    }


    /**
     * Register the ContentObserver.
     */
    protected void registerContentObserver() {
        // Register a ContentObserver that's notified when Contacts
        // change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().registerContentObserver
                (CheckInContract.CheckInEntry.CONTENT_URI,
                        true,
                        checkinChangeContentObserver);
    }

    /**
     * Unregister the ContentObserver.
     */
    protected void unregisterContentObserver() {
        // Unregister a ContentObserver so it won't be notified when
        // Contacts change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().unregisterContentObserver
                (checkinChangeContentObserver);
    }

    public SimpleCursorAdapter makeCursorAdapter() {
        return mCursorAdapter;
    }

    /**
     * Display the contents of the cursor as a ListView.
     */
    public void displayCursor(Cursor cursor) {
        // Store the most recent result from a query so the display
        // can be updated after a runtime configuration change.
        mCursor = cursor;

        // Display the designated columns in the cursor as a List in
        // the ListView connected to the SimpleCursorAdapter.
        mCursorAdapter.changeCursor(cursor);
    }


    public Activity getActivity(){
        return mActivity.get();
    }


    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }

}
