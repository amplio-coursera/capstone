package com.amplioapps.capstonebeta.presenter;

import android.app.Activity;
import android.content.Intent;

import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.model.auth.SignupAccountDetails;
import com.amplioapps.capstonebeta.view.SignupActivity;

import java.lang.ref.WeakReference;


public class SignupOps implements ConfigurableOps {

    protected final static String TAG =
            SignupOps.class.getSimpleName();

    protected WeakReference<SignupActivity> mActivity;

    private SignupCommand mSignupCommand;

    private TakePictureCommand mTakePictureCommand;

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((SignupActivity) activity);

        if (firstTimeIn){
            mSignupCommand = new SignupCommand(this);
            mTakePictureCommand = new TakePictureCommand(this);
        }
    }

    public void runTakePicture() {
        mTakePictureCommand.execute();
    }

    public void runSignup(SignupAccountDetails details) {
        mSignupCommand.execute(details);
    }

    public void cancel() {
        getActivity().setResult(getActivity().RESULT_CANCELED);
        getActivity().finish();
    }

    public Activity getActivity(){
        return mActivity.get();
    }
}
