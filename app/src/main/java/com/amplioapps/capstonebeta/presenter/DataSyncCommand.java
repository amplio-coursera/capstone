package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.sync.GotItSyncAdapter;


public class DataSyncCommand implements Command<Account> {

    private ContextualOps mOps;


    public DataSyncCommand(ContextualOps ops) {
        mOps = ops;
    }

    @Override
    public void execute(Account account) {
        // Toast Message as placeholder
        Utils.showToast(mOps.getApplicationContext(),
                "Sync Content Provider");

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        ContentResolver.requestSync(account,
                mOps.getApplicationContext()
                        .getString(R.string.content_authority),
                bundle);
    }

}
