package com.amplioapps.capstonebeta.presenter;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.amplioapps.capstonebeta.commons.GenericActivity;
import com.amplioapps.capstonebeta.view.ChartActivity;
import com.amplioapps.capstonebeta.view.CheckInActivity;

public class CheckinAlarmNotificationReceiver extends BroadcastReceiver{

    // Set up Variables
    private static final int MY_NOTIFICATION_ID = 1;

    private final CharSequence mTickerTitle = "Checkin Time!";
    private final CharSequence mContentTitle = "Diabetes Checkin ";
    private final CharSequence mContentText = "Are you ready to prick the voodoo doll?";

    private Intent mNotificationIntent;
    private PendingIntent mContentIntent;
    private Notification mNotification;


    @Override
    public void onReceive(Context context, Intent intent) {

        // Intent to be used when user clicks Notification View
        mNotificationIntent = new Intent(context, CheckInActivity.class);

        // Pending Intent
        mContentIntent = PendingIntent.getActivity(context, 0,
                mNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Build Notification
        Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setTicker(mTickerTitle)
                .setSmallIcon(android.R.drawable.sym_def_app_icon)
                .setAutoCancel(true)
                .setContentTitle(mContentTitle)
                .setContentText(mContentText)
                .setContentIntent(mContentIntent);

        mNotification = notificationBuilder.build();

        // Get Notification Manager
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Pass Notification to Notification Manager
        notificationManager.notify(MY_NOTIFICATION_ID, mNotification);

    }
}