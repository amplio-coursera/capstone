package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.util.Log;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.auth.WebSvcConfig;
import com.amplioapps.capstonebeta.view.LoginActivity;

import java.lang.ref.WeakReference;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_AUTH_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.PARAM_USER_PASS;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_IS_ADDING_NEW_ACCOUNT;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;


public class FinishLoginCommand implements Command<Intent> {

    private final String TAG = this.getClass().getSimpleName();

    protected WeakReference<LoginActivity> mActivity;

    private ContextualOps mOps;
    private AccountManager mAccountManager;
    private String mAuthTokenType;
    private boolean mAddAccount;


    public FinishLoginCommand(ContextualOps loginOps) {
        mOps = loginOps;

        mActivity = new WeakReference<>(
                (LoginActivity) mOps.getActivity());

        mAccountManager = AccountManager.get(mOps.getActivity().getBaseContext());

        mAuthTokenType = mOps.getActivity().getIntent()
                .getStringExtra(ARG_AUTH_TYPE);

        mAddAccount = mOps.getActivity().getIntent()
                .getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false);

        if (mAuthTokenType == null)
            mAuthTokenType = WebSvcConfig.TOKEN_TYPE_FULL;
    }

    @Override
    public void execute(Intent intent) {
        Log.d(TAG, "> FinishLogin");

        final String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        final String accountPassword = intent.getStringExtra(PARAM_USER_PASS);

        final Account account = new Account(accountName,
                intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

        if (mAddAccount) {
            Log.d(TAG, "> FinishLogin > addAccountExplicitly");
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = mAuthTokenType;

            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account, accountPassword,
                    intent.getBundleExtra(AccountManager.KEY_USERDATA));
            mAccountManager.setAuthToken(account, authtokenType, authtoken);
        } else {
            Log.d(TAG, "> FinishLogin > setPassword");
            mAccountManager.setPassword(account, accountPassword);
        }

        sCurrentAccountName = accountName;

        mActivity.get().setAccountAuthenticatorResult(intent.getExtras());
        mActivity.get().setResult(mActivity.get().RESULT_OK, intent);
        mActivity.get().finish();
    }
}
