package com.amplioapps.capstonebeta.presenter;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.model.CheckInItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.filter.Approximator;
import com.github.mikephil.charting.data.filter.Approximator.ApproximatorType;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;


public class ConvertCheckinsCommand implements Command<Integer> {

    // VARIABLES
    private ChartOps mOps;
    private ArrayList<CheckInItem> mCheckInItems;
    private static final Integer BLOOD_SUGAR = 0;
    private static final Integer INSULIN = 1;
    private LineData mData;

    // Constructor
    public ConvertCheckinsCommand(ChartOps ops) {

        mOps = ops;

    }

    @Override
    public void execute(Integer param) {

        mCheckInItems = mOps.getCheckIns();

        // Variables for the method
        long questionID;
        String title;
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();

        // Switch Case
        // Question ID and Title Based on param

        switch (param){

            case 0:{
                // User chose blood sugar
                questionID = 1L;
                title = "Blood Sugar";
                break;
            }
            case 1:{
                // User chose insulin
                questionID = 2L;
                title = "Insulin";
                break;

            }default:{
                // Defaults to Blood Sugar
                questionID = 1L;
                title = "Blood Sugar";
                break;
            }
        }

        if (mCheckInItems != null) {
            // Loop through the ArrayList
            for (int i = 0; i < mCheckInItems.size(); i++) {

                CheckInItem checkInItem = mCheckInItems.get(i);

                Date date = new Date(checkInItem.getTimestamp());
                String formattedDate = new SimpleDateFormat("MM/dd/yyyy, KK:mm a").format(date);

                // Create X Values
                xVals.add(formattedDate);

                // Create Y Values
                int answer = Integer.parseInt(checkInItem.getAnswer(questionID));
                Entry entry = new Entry(answer, i);
                yVals.add(entry);
            }

            // Create Data Object
            LineDataSet set1 = new LineDataSet(yVals, title);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            mData = new LineData(xVals, dataSets);

            // Set Data Object in Ops
            mOps.setChartData(mData);
        }
    }
}
