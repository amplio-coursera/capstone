package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;

import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.model.auth.AccountConfig;
import com.amplioapps.capstonebeta.view.ChartActivity;
import com.github.mikephil.charting.data.LineData;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;


public class ChartOps implements ConfigurableOps, ContextualOps {

    // Debugging Tag
    protected final static String TAG = LoginOps.class.getSimpleName();

    // Weak Reference and Command Object
    protected WeakReference<ChartActivity> mActivity;
    private ChartDataUpdateCommand mChartDataUpdateCommand;
    private CheckinAlarmCommand mCheckinAlarmCommand;
    private SampleCheckinsCommand mSampleCheckinsCommand;
    private ConvertCheckinsCommand mConvertCheckinsCommand;

    // Member Variables
    private Cursor mDataCursor;
    private ArrayList<CheckInItem> mCheckinItems;
    private long mCurrentTime;
    private LineData mChartData;

    private AccountManager mAccountManager;
    private Account mAccount;
    private String mUserid;

    // Default alarm time
    private static final long INITIAL_ALARM_DELAY = 1*30*1000L; // 30 secs



    @Override
    public void onConfiguration(Activity activity, boolean firstTimeIn) {

        // Instantiate weak reference
        mActivity = new WeakReference<>((ChartActivity) activity);

        // Check if it's first time in
        if (firstTimeIn){
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();

            mUserid = mAccountManager.getUserData(
                    mAccount,
                    AccountConfig.USERDATA_USER_OBJ_ID);

            initialize();
        }
    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    // Initialize Method
    private void initialize(){
        mChartDataUpdateCommand = new ChartDataUpdateCommand(this);
        mCheckinAlarmCommand = new CheckinAlarmCommand();
        mSampleCheckinsCommand = new SampleCheckinsCommand();
        mConvertCheckinsCommand = new ConvertCheckinsCommand(this);
        mCurrentTime = System.currentTimeMillis();
    }

    // Command Process: Update Data
    public void runChartDataUpdate(){
        mChartDataUpdateCommand.execute(mUserid);
    }

    // Command Process: Set Alarm
    public void runCheckinAlarm(){

        // Set time
        mCheckinAlarmCommand.setInitialDelay(INITIAL_ALARM_DELAY);
        // Execute Command
        mCheckinAlarmCommand.execute(this);
    }

    // Command Process:  Convert  Cursor to ArrayList of CheckInItems

    // Command Process:  Create Dummy ArrayList of CheckInItems for testing
    public void runSampleCheckins(){
        mSampleCheckinsCommand.execute(this);
    }

    // Command Process:  Convert  ArrayList of CheckInItems to Data Object for Chart
    public void runConvertCheckInsToChartData(int position){
        mConvertCheckinsCommand.execute(new Integer(position));
    }




    // Getter and Setter for Cursor
    public Cursor getCursor(){
        return mDataCursor;
    }
    public void setCursor(Cursor cursor){
        mDataCursor = cursor;
    }

    // Getter and Setter for ArrayList
    public ArrayList<CheckInItem> getCheckIns(){
        return mCheckinItems;
    }
    public void setCheckIns(ArrayList<CheckInItem> checkInItems){
        mCheckinItems = checkInItems;
    }

    // Getter and Setter for Current Time
    public long getCurrentTime(){
        return mCurrentTime;
    }
    public void setCurrentTime(long currentTime){
        mCurrentTime = currentTime;
    }

    // Getter and Setter for Current Time
    public LineData getChartData(){
        return mChartData;
    }
    public void setChartData(LineData chartData){
        mChartData = chartData;
    }



    @Override
    public Context getApplicationContext() {
        return mActivity.get().getApplicationContext();
    }

    @Override
    public Activity getActivity() {
        return mActivity.get();
    }
}
