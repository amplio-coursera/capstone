package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInListAdapter;
import com.amplioapps.capstonebeta.model.SimpleUser;
import com.amplioapps.capstonebeta.model.UserListAdapter;
import com.amplioapps.capstonebeta.model.auth.AccountConfig;
import com.amplioapps.capstonebeta.view.FollowerActivity;

import java.lang.ref.WeakReference;
import java.util.Collection;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;


public class FollowerOps implements ConfigurableOps, ContextualOps {

    protected final String TAG =
            this.getClass().getSimpleName();

    protected WeakReference<FollowerActivity> mActivity;

    protected AccountManager mAccountManager;
    protected Account mAccount;

    private UserListAdapter mCursorAdapter;
    private Cursor mCursor;
    private ContentResolver cr;

    protected final ContentObserver followerChangeContentObserver =
            new ContentObserver(new Handler()) {
                /**
                 * Trigger a query and display the results.
                 */
                @Override
                public void onChange (boolean selfChange) {
                    queryFollowers();
                }
            };

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((FollowerActivity) activity);

        if (firstTimeIn){
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();

            // Initialize the SimpleCursorAdapter.
            mCursorAdapter =
                    new UserListAdapter(
                            UserListAdapter.FOLLOWER_LIST_TYPE,
                            getApplicationContext(),
                            R.layout.user_item,
                            null,
                            CheckInContract.sFollowerUserColumnsToDisplay,
                            CheckInContract.sFollowerUserColumnResIds,
                            1);

            cr = getApplicationContext().getContentResolver();

            // Unregister the ContentObserver.
            unregisterContentObserver();

            // Register the ContentObserver.
            registerContentObserver();
        } else if (mCursor != null)
            // Redisplay the contents of the cursor after a runtime
            // configuration change.
            displayCursor(mCursor);

    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public void showFollowers(){
        displayCursor(queryFollowers());
    }


    private Cursor queryFollowers(){

        String userid = mAccountManager.getUserData(
                mAccount,
                AccountConfig.USERDATA_USER_OBJ_ID);


        return cr.query(CheckInContract.FOLLOWER_USER_URI,
                null,
                null,
                new String[] { userid },
                null);
    }

    /**
     * Register the ContentObserver.
     */
    protected void registerContentObserver() {
        // Register a ContentObserver that's notified when Contacts
        // change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().registerContentObserver
                (CheckInContract.CheckInEntry.CONTENT_URI,
                        true,
                        followerChangeContentObserver);
    }

    /**
     * Unregister the ContentObserver.
     */
    protected void unregisterContentObserver() {
        // Unregister a ContentObserver so it won't be notified when
        // Contacts change (e.g., are inserted, modified, or deleted).
        getActivity().getContentResolver().unregisterContentObserver
                (followerChangeContentObserver);
    }

    public SimpleCursorAdapter makeCursorAdapter() {
        return mCursorAdapter;
    }

    /**
     * Display the contents of the cursor as a ListView.
     */
    public void displayCursor(Cursor cursor) {
        // Store the most recent result from a query so the display
        // can be updated after a runtime configuration change.
        mCursor = cursor;

        // Display the designated columns in the cursor as a List in
        // the ListView connected to the SimpleCursorAdapter.
        mCursorAdapter.changeCursor(cursor);
    }


    public void addFollower(String username) {
        // Columns to query.
        final String columnsToQuery[] =
                new String[] {
                        CheckInContract.UsersEntry._ID,
                        CheckInContract.UsersEntry.COLUMN_USERID,
                        CheckInContract.UsersEntry.COLUMN_USERNAME
                };

        // Contacts to select.
        final String selection =
                "(("
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " == "
                        + username
                        + "))";

        Cursor cursor = cr.query(CheckInContract.UsersEntry.CONTENT_URI,
                columnsToQuery,
                selection,
                null,
                null);

        if(cursor.getCount() > 0) {
            Collection<SimpleUser> users = SimpleUser.fromCursor(cursor);

            SimpleUser user = getUser(users, username);

            if (user != null){
                String userid = mAccountManager.getUserData(
                        mAccount,
                        AccountConfig.USERDATA_USER_OBJ_ID);

                ContentValues followerCvs = new ContentValues();
                followerCvs.put(CheckInContract.FollowerEntry.COLUMN_OWNERID, userid);
                followerCvs.put(CheckInContract.FollowerEntry.COLUMN_FOLLOWERID, user.getId());

                cr.insert(CheckInContract.FollowerEntry.CONTENT_URI,
                        followerCvs);

                ContentValues followingCvs = new ContentValues();
                followingCvs.put(CheckInContract.FollowingEntry.COLUMN_OWNERID, user.getId());
                followingCvs.put(CheckInContract.FollowingEntry.COLUMN_FOLLOWINGID, userid);

                cr.insert(CheckInContract.FollowingEntry.CONTENT_URI,
                        followingCvs);

                Utils.showToast(getApplicationContext(),
                        "User added: " + username);
                return;
            }
        }

        Utils.showToast(getApplicationContext(),
                "User not found: " + username);
    }

    private SimpleUser getUser(Collection<SimpleUser> users, String username){
        for(SimpleUser user : users) {
            Log.d(TAG, "> Username: " + user.getUsername());
            if(user.getUsername().equals(username)){
                return user;
            }
        }
        return null;
    }

    public Activity getActivity(){
        return mActivity.get();
    }


    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }
}
