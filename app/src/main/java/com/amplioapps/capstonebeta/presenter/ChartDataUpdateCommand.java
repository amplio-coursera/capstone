package com.amplioapps.capstonebeta.presenter;

import android.content.ContentResolver;
import android.database.Cursor;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.GenericAsyncTask;
import com.amplioapps.capstonebeta.commons.GenericAsyncTaskOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ChartDataUpdateCommand extends GenericAsyncTaskOps<String, Void, Cursor>
        implements Command<String> {

    // Reference to Ops and Content Resolver
    private ChartOps mOps;
    private ContentResolver mCR;

    // Constructor
    public ChartDataUpdateCommand(ChartOps ops){

        // Instantiate variables
        mOps = ops;
        mCR = ops.getApplicationContext().getContentResolver();
    }

    @Override
    public void execute(String param) {

        // Instantiate AsyncTask
        final GenericAsyncTask<String,
                Void,
                Cursor,
                ChartDataUpdateCommand> asyncTask =
                new GenericAsyncTask<>(this);

        // Execute AsyncTask
        asyncTask.execute(param);

    }

    @Override
    public Cursor doInBackground(String... params) {


        // (1) Build proper query
        // (2) Query SQLite DB
        // (3) Return Cursor
        return mCR.query(CheckInContract.CHECKIN_ANSWER_URI,
                null, null, new String[] { params[0] }, null);

    }

    @Override
    public void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);

        /*
        if(cursor != null){
            Utils.showToast(mOps.getApplicationContext(),
                    "We got a cursor!");
        }
        */

        mOps.setCheckIns(
                new ArrayList<CheckInItem>(
                        CheckInItem.fromCursor(cursor)));
    }

}
