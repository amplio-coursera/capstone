package com.amplioapps.capstonebeta.presenter;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.GenericAsyncTask;
import com.amplioapps.capstonebeta.commons.GenericAsyncTaskOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.auth.SignupAccountDetails;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.USERDATA_USER_OBJ_ID;
import static com.amplioapps.capstonebeta.model.auth.WebSvcConfig.sWebSvcAuth;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.PARAM_USER_PASS;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.KEY_ERROR_MESSAGE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_ACCOUNT_TYPE;


public class LoginCommand extends GenericAsyncTaskOps<String, Void, Intent>
        implements Command<String[]> {


    private final String TAG = this.getClass().getSimpleName();

    private LoginOps mOps;
    private String mAccountType;

    private FinishLoginCommand finishLoginCommand;

    public LoginCommand(LoginOps loginOps) {
        mOps = loginOps;

        finishLoginCommand = new FinishLoginCommand(mOps);

        mAccountType = mOps.getActivity()
                .getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
    }

    @Override
    public void execute(String... params) {

        final GenericAsyncTask<String,
                Void,
                Intent,
                LoginCommand> asyncTask =
                new GenericAsyncTask<>(this);

        asyncTask.execute(params);
    }

    /**
     * Run in a background Thread to avoid blocking the UI Thread.
     */
    @SuppressWarnings("unchecked")
    @Override
    public Intent doInBackground(String... params) {
        Log.d(TAG, "Started authenticating");

        String username  = params[0];
        String password  = params[1];

        String authtoken = null;
        Bundle data = new Bundle();
        try {
            authtoken = sWebSvcAuth.signIn(username, password);

            Log.d(TAG, "AUTH TOKEN:"+ authtoken);

            data.putString(AccountManager.KEY_ACCOUNT_NAME, username);
            data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType);
            data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
            data.putString(PARAM_USER_PASS, password);

            Bundle userData = new Bundle();
            userData.putString(USERDATA_USER_OBJ_ID, String.valueOf(sWebSvcAuth.getUserId()));
            data.putBundle(AccountManager.KEY_USERDATA, userData);

        } catch (Exception e) {
            data.putString(KEY_ERROR_MESSAGE, e.getMessage());
        }

        return new Intent().putExtras(data);

    }

    @Override
    public void onPostExecute(Intent intent) {
        if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
            Utils.showToast(mOps.getApplicationContext(),
                    intent.getStringExtra(KEY_ERROR_MESSAGE));
        } else {
            finishLoginCommand.execute(intent);
        }
    }

}
