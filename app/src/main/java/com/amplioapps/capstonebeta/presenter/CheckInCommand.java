package com.amplioapps.capstonebeta.presenter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;

import com.amplioapps.capstonebeta.commons.Command;
import com.amplioapps.capstonebeta.commons.GenericAsyncTask;
import com.amplioapps.capstonebeta.commons.GenericAsyncTaskOps;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.view.CheckInActivity;

import java.lang.ref.WeakReference;
import java.util.Map;


public class CheckInCommand extends GenericAsyncTaskOps<CheckInItem, Void, Integer>
        implements Command<CheckInItem> {

    protected WeakReference<CheckInActivity> mActivity;

    private CheckInOps mOps;

    private ContentResolver mCR;

    public CheckInCommand(CheckInOps checkInOps){
        mOps = checkInOps;

        mActivity = new WeakReference<>(
                (CheckInActivity) mOps.getActivity());

        mCR = checkInOps.getApplicationContext().getContentResolver();
    }

    @Override
    public void execute(CheckInItem checkInItem) {
        // Add Checkin to Content Provider here
        // Create a GenericAsyncTask to insert the contacts off the UI
        // Thread.
        final GenericAsyncTask<CheckInItem,
                Void,
                Integer,
                CheckInCommand> asyncTask =
                new GenericAsyncTask<>(this);

        // Execute the GenericAsyncTask.
        asyncTask.execute(checkInItem);
    }

    /**
     * Run in a background Thread to avoid blocking the UI Thread.
     */
    @SuppressWarnings("unchecked")
    @Override
    public Integer doInBackground(CheckInItem... checkInItems) {
        // Insert all the contacts designated by the Iterator.
        return insertCheckInItem(checkInItems[0]);
    }

    /**
     * A count of the number of results in the query are displayed in
     * the UI Thread.
     */
    @Override
    public void onPostExecute(Integer count) {
        // Toast Message as placeholder
        Utils.showToast(mOps.getApplicationContext(),
                "Check In completed");

        mActivity.get().setResult(mActivity.get().RESULT_OK, new Intent());
        mActivity.get().finish();
    }


    private Integer insertCheckInItem(CheckInItem checkInItem) {
        long id = insertCheckIn(checkInItem);
        insertAnswers(id, checkInItem.getResponses());

        return 1;
    }

    private void insertAnswers(long checkInId, Map<Long, String> responses) {

        for (Map.Entry<Long, String> entry : responses.entrySet()) {
            long questionId = entry.getKey();
            String answer = entry.getValue();

            ContentValues cvs = new ContentValues();

            cvs.put(CheckInContract.AnswerEntry.COLUMN_QUESTIONID,
                    questionId);
            cvs.put(CheckInContract.AnswerEntry.COLUMN_CHECKINID,
                    checkInId);
            cvs.put(CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT,
                    answer);

            mCR.insert(CheckInContract.AnswerEntry.CONTENT_URI,
                    cvs);
        }
    }

    private long insertCheckIn(CheckInItem checkinItem) {

        ContentValues cvs = new ContentValues();

        cvs.put(CheckInContract.CheckInEntry.COLUMN_OWNER,
                checkinItem.getOwnerId());

        cvs.put(CheckInContract.CheckInEntry.COLUMN_TIMESTAMP,
                checkinItem.getTimestamp());

        Uri result = mCR.insert(CheckInContract.CheckInEntry.CONTENT_URI,
                cvs);

        return ContentUris.parseId(result);
    }

}
