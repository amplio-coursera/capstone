package com.amplioapps.capstonebeta.presenter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;

import com.amplioapps.capstonebeta.commons.ConfigurableOps;
import com.amplioapps.capstonebeta.commons.ContextualOps;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.view.CheckInActivity;
import com.amplioapps.capstonebeta.view.CheckInDetailActivity;

import java.lang.ref.WeakReference;
import java.util.Collection;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;

public class CheckInDetailOps implements ConfigurableOps, ContextualOps {

    protected final static String TAG =
            LoginOps.class.getSimpleName();

    protected WeakReference<CheckInDetailActivity> mActivity;

    private AccountManager mAccountManager;
    private Account mAccount;
    private Cursor mCursor;
    private ContentResolver cr;

    public void onConfiguration(Activity activity,
                                boolean firstTimeIn) {

        mActivity = new WeakReference<>((CheckInDetailActivity) activity);

        if (firstTimeIn){
            mAccountManager = AccountManager.get(mActivity.get().getBaseContext());
            setAccount();
            cr = getApplicationContext().getContentResolver();
        }
    }

    public CheckInItem getCheckInItem(String checkinId){
        Cursor cursor = cr
                .query(CheckInContract.CHECKIN_ANSWER_URI,
                        null,
                        null,
                        null,
                        null);

        Collection<CheckInItem> checkInItems =
                CheckInItem.fromCursor(cursor);

        for(CheckInItem item : checkInItems){
            if(item.getResponseId() == Long.parseLong(checkinId)){
                return item;
            }
        }
        return null;
    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public Activity getActivity(){
        return mActivity.get();
    }

    public Context getApplicationContext(){
        return mActivity.get().getApplicationContext();
    }
}
