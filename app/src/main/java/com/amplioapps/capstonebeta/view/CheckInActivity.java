package com.amplioapps.capstonebeta.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GenericActivity;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.presenter.CheckInOps;

import java.util.Date;


public class CheckInActivity extends GenericActivity<CheckInOps> {

    private final int GLUCOSE_MAX = 700;
    private final int GLUCOSE_MIN = 0;
    private final int GLUCOSE_DEFAULT = 120;

    private final int INSULIN_MAX = 500;
    private final int INSULIN_MIN = 0;
    private final int INSULIN_DEFAULT = 10;


    NumberPicker mGlucoseNoPicker = null;
    NumberPicker mInsulinNoPicker = null;

    private EditText mTxtMeter =  null;
    private EditText mTxtInsulin =  null;
    private EditText mTxtMeal =  null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, CheckInOps.class);
        setContentView(R.layout.checkin);
        initialize();
    }

    private void initialize() {

        mTxtMeter =  (EditText) findViewById(R.id.meterEntry);
        mTxtInsulin =  (EditText) findViewById(R.id.insulinEntry);
        mTxtMeal =  (EditText) findViewById(R.id.mealEntry);
    }

    public void checkIn(View view){
        getOps().runCheckIn(
                mTxtMeter.getText().toString(),
                mTxtInsulin.getText().toString(),
                mTxtMeal.getText().toString());
    }

    public void cancelCheckIn(View view)
    {
        getOps().cancelCheckIn();
    }
}
