package com.amplioapps.capstonebeta.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.presenter.FollowerOps;
import com.amplioapps.capstonebeta.presenter.FollowingOps;


public class FollowingActivity extends GotItBaseActivity<FollowingOps> {

    private ListView mListView;
    private Button mAddButton;
    private EditText mTxtAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, FollowingOps.class);
        setContentView(R.layout.user_list);
        initialize();

        getOps().showFollowers();
    }

    private void initialize() {
        setTitle("Users you are following");

        // Initialize the List View.
        mListView = (ListView) findViewById(R.id.userlist);
        mListView.setAdapter(getOps().makeCursorAdapter());
        mTxtAddUser = (EditText) findViewById(R.id.etAddUserEntry);
        mAddButton = (Button) findViewById(R.id.buttonAdd);

        mTxtAddUser.setVisibility(View.GONE);
        mAddButton.setVisibility(View.GONE);
    }
}
