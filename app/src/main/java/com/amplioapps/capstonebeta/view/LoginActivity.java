package com.amplioapps.capstonebeta.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GenericAuthenticatorActivity;
import com.amplioapps.capstonebeta.presenter.LoginOps;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_ACCOUNT_NAME;


public class LoginActivity extends GenericAuthenticatorActivity<LoginOps> {

    private final String TAG = this.getClass().getSimpleName();

    private TextView txtUsername;
    private TextView txtPassword;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, LoginOps.class);

        setContentView(R.layout.login);
        initialize();
    }

    private void initialize() {
        txtUsername = (TextView) findViewById(R.id.textUsername);
        txtPassword = (TextView) findViewById(R.id.textPassword);

        final String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);

        if (accountName != null) {
            txtUsername.setText(accountName);
        }
    }

    public void register(View v) {
        getOps().onUserRegister();
    }

    public void login(View v) {
        final String userName = txtUsername.getText().toString();
        final String userPass = txtPassword.getText().toString();

        getOps().onUserLogin(userName, userPass);
    }
}
