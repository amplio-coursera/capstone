package com.amplioapps.capstonebeta.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GenericActivity;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.presenter.CheckInDetailOps;


public class CheckInDetailActivity extends GenericActivity<CheckInDetailOps> {

    public static final String ARG_CHECKIN_DETAIL_ID = "checkin_detail_id";

    private TextView mTxtMeter =  null;
    private TextView mTxtInsulin =  null;
    private TextView mTxtMeal =  null;

    private CheckInItem mCheckin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, CheckInDetailOps.class);
        setContentView(R.layout.checkin_detail);

        Intent intent = getIntent();
        String checkinid = intent.getStringExtra(ARG_CHECKIN_DETAIL_ID);

        mCheckin = getOps().getCheckInItem(checkinid);

        if(mCheckin != null) {
            initialize();
        }
    }

    private void initialize() {
        mTxtMeter =  (TextView) findViewById(R.id.detailmeterEntry);
        mTxtInsulin =  (TextView) findViewById(R.id.detail_insulinEntry);
        mTxtMeal =  (TextView) findViewById(R.id.detail_mealEntry);

        mTxtMeter.setText(mCheckin.getAnswer(1L));
        mTxtInsulin.setText(mCheckin.getAnswer(2L));
        mTxtMeal.setText(mCheckin.getAnswer(3L));
    }


    public void closeDetail(View view)
    {
        setResult(RESULT_CANCELED);
        finish();
    }
}
