package com.amplioapps.capstonebeta.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.presenter.CheckInListOps;
import com.amplioapps.capstonebeta.presenter.FollowListOps;

public class FollowListActivity extends GotItBaseActivity<FollowListOps> {

    public static final String ARG_FOLLOWING_USER = "userid";

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, FollowListOps.class);
        setContentView(R.layout.checkin_list);
        initialize();

        Intent intent = getIntent();
        String userid = intent.getStringExtra(ARG_FOLLOWING_USER);

        getOps().showCheckins(userid);

    }


    private void initialize() {
        setTitle("Recent Checkins");

        // Initialize the List View.
        mListView = (ListView) findViewById(R.id.listView);
        mListView.setAdapter(getOps().makeCursorAdapter());
    }
}
