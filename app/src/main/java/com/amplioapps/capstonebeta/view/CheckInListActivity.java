package com.amplioapps.capstonebeta.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.presenter.CheckInListOps;


public class CheckInListActivity extends GotItBaseActivity<CheckInListOps> {

    private static final int REQ_CHECKIN = 2;

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, CheckInListOps.class);
        setContentView(R.layout.checkin_list);
        initialize();

        getOps().showCheckins();
    }


    private void initialize() {
        setTitle("Recent Checkins");

        // Initialize the List View.
        mListView = (ListView) findViewById(R.id.listView);
        mListView.setAdapter(getOps().makeCursorAdapter());
    }

    public void addCheckIn(View view){
        Intent intent = new Intent(
                this, CheckInActivity.class);

        if (getIntent().getExtras() != null)
            intent.putExtras(getIntent().getExtras());

        startActivityForResult(intent, REQ_CHECKIN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

}
