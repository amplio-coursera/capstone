package com.amplioapps.capstonebeta.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GenericActivity;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.commons.Utils;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.presenter.ChartOps;
import com.amplioapps.capstonebeta.presenter.CheckinAlarmNotificationReceiver;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;

import java.util.ArrayList;


public class ChartActivity extends GotItBaseActivity<ChartOps>
        implements AdapterView.OnItemSelectedListener{

    // Variables
    private Spinner mDropdown;
    private LineChart mChart;

    /*
    // Variables for Alarm and Broadcast Receiver
    private AlarmManager mAlarmManager;
    private Intent mCheckinAlarmNotifRecIntent;
    private PendingIntent mCheckinAlarmNotifRecPendingIntent;
    private long mInitialAlarmDelay;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Pass Ops object to Generic Activity
        super.onCreate(savedInstanceState, ChartOps.class);

        // Set Content View
        setContentView(R.layout.chart);

        // Initialize UI Elements
        initialize();

        // Set Reminder
        // Hard-coding 1 Minute for now (should be based on settings)
        setReminder();
        //setReminderInActivity();


        // Query Data Base
        updateChartData(new Integer(0));

    }

    // CUSTOM METHODS
    // Initialize Method
    private void initialize(){

        // Create Spinner
        mDropdown = (Spinner) findViewById(R.id.feedback_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.diabetes_metric,
                android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDropdown.setAdapter(adapter);
        mDropdown.setOnItemSelectedListener(this);

        // Instantiate Chart
        mChart = (LineChart) findViewById(R.id.chart1);


    }

    // REMINDER LOGIC
    private void setReminder(){

        // Bridge Pattern
        getOps().runCheckinAlarm();

    }



    /*
    private void setReminderInActivity(){

        mInitialAlarmDelay = 1*30*1000L; // 30 secs

        // Get Alarm Service
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Create Intent to Broadcast
        mCheckinAlarmNotifRecIntent = new Intent(ChartActivity.this, CheckinAlarmNotificationReceiver.class);

        // Create Pending Intent
        mCheckinAlarmNotifRecPendingIntent = PendingIntent.getBroadcast(ChartActivity.this, 0,
                mCheckinAlarmNotifRecIntent, 0);

        // Set Repeating Alarm
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + mInitialAlarmDelay,
                2*60*1000,
                mCheckinAlarmNotifRecPendingIntent);



    }

    */


    // Item Selected Listener
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // Query Data Base
        updateChartData(new Integer(position));

        // For Debugging Purposes, create sample data
        //createSampleData();

        // Convert ArrayList to Chart Data Object
        convertToChartDate(position);

        // Update Chart (call get method for data object)
        updateChart(getOps().getChartData());

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // Update Data
    // Calls to SQLite DB with the proper query
    // Brings back the correct Cursor
    // Bridge pattern.  Forward business Logic to Ops Object
    // Logic built in the command object:
    // (1) Build proper query
    // (2) Call Update Data Method
    // (3) Instantiate the proper Metric Object
    // (4) Update Chart
    private Cursor updateChartData(Integer metric){

        getOps().runChartDataUpdate();
        return getOps().getCursor();

    }

    // Bridge Pattern:  Call Ops Command to create sample data
    private void createSampleData(){
        getOps().runSampleCheckins();
    }

    // Bridge Pattern:  Call Ops Command to convert Array Object to Chart data
    private void convertToChartDate(int metric){
        getOps().runConvertCheckInsToChartData(metric);
    }

    // Update Chart
    // Call Update Data
    // Get Cursor Back
    // Process Data in Cursor
    private void updateChart(LineData data){
        mChart.setData(data);

        mChart.invalidate();

    }

    /*
    // MENU METHODS
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gotit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Write switch case for each view id

        int id = item.getItemId();
        if (id == R.id.action_checkin) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */


}







