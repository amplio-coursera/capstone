package com.amplioapps.capstonebeta.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GenericActivity;
import com.amplioapps.capstonebeta.model.auth.SignupAccountDetails;
import com.amplioapps.capstonebeta.presenter.SignupOps;
import com.amplioapps.capstonebeta.presenter.TakePictureCommand;

import java.util.Calendar;
import java.util.Date;

public class SignupActivity extends GenericActivity<SignupOps> {

    private EditText mTxtUsername;
    private EditText mTxtPassword;
    private EditText mTxtFirstName;
    private EditText mTxtLastName;

    private TextView tvDisplayDate;

    private int year;
    private int month;
    private int day;

    static final int DATE_DIALOG_ID = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, SignupOps.class);

        // Set Content View
        setContentView(R.layout.signup);

        mTxtUsername = (EditText) findViewById(R.id.textUsername);
        mTxtPassword = (EditText) findViewById(R.id.textPassword);
        mTxtFirstName = (EditText) findViewById(R.id.textFirstname);
        mTxtLastName = (EditText) findViewById(R.id.textLastname);
        tvDisplayDate = (EditText) findViewById(R.id.textBirthDate);

        setCurrentDateOnView();
        addListenerOnTextBox();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TakePictureCommand.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageButton ib = (ImageButton) findViewById(R.id.imageUser);
            ib.setImageBitmap(imageBitmap);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }


    public void takePicture(View v) {
        getOps().runTakePicture();
    }


    public void signup(View v) {
        Calendar c1 = Calendar.getInstance();
        c1.set(year, month-1, day);
        Date sDate = c1.getTime();

        getOps().runSignup(
                new SignupAccountDetails(
                        mTxtFirstName.getText().toString(),
                        mTxtLastName.getText().toString(),
                        mTxtUsername.getText().toString(),
                        mTxtPassword.getText().toString(),
                        tvDisplayDate.getText().toString(),
                        sDate.getTime()));
    }

    public void cancel(View v) {
        getOps().cancel();
    }

    public void addListenerOnTextBox() {


        tvDisplayDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);

            }

        });

    }

    // display current date
    public void setCurrentDateOnView() {

        tvDisplayDate = (TextView) findViewById(R.id.textBirthDate);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        tvDisplayDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(month + 1).append("-").append(day).append("-")
                .append(year).append(" "));

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month,day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview
            tvDisplayDate.setText(new StringBuilder().append(month + 1)
                    .append("-").append(day).append("-").append(year)
                    .append(" "));
        }
    };

}
