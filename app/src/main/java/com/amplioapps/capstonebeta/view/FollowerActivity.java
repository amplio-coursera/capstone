package com.amplioapps.capstonebeta.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.commons.GotItBaseActivity;
import com.amplioapps.capstonebeta.presenter.FollowerOps;


public class FollowerActivity extends GotItBaseActivity<FollowerOps> {

    private ListView mListView;
    private EditText mTxtAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, FollowerOps.class);
        setContentView(R.layout.user_list);
        initialize();

        getOps().showFollowers();
    }

    private void initialize() {
        setTitle("Users following you");

        // Initialize the List View.
        mListView = (ListView) findViewById(R.id.userlist);
        mListView.setAdapter(getOps().makeCursorAdapter());
        mTxtAddUser = (EditText) findViewById(R.id.etAddUserEntry);
    }

    public void addFollower(View view){
        getOps().addFollower(mTxtAddUser.getText().toString());
    }

}
