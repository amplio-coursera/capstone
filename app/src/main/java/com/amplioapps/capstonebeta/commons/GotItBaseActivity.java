package com.amplioapps.capstonebeta.commons;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.presenter.DataSyncCommand;
import com.amplioapps.capstonebeta.view.ChartActivity;
import com.amplioapps.capstonebeta.view.CheckInListActivity;
import com.amplioapps.capstonebeta.view.FollowerActivity;
import com.amplioapps.capstonebeta.view.FollowingActivity;
import com.amplioapps.capstonebeta.view.LoginActivity;

import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_ACCOUNT_TYPE;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.ARG_IS_ADDING_NEW_ACCOUNT;
import static com.amplioapps.capstonebeta.model.auth.AccountConfig.sCurrentAccountName;

/**
 * This base activity abstracts away the recurring features like
 * user menu and user account management.
 */
public class GotItBaseActivity<BaseOps extends ConfigurableOps>
        extends GenericActivity<BaseOps> {

    protected int REQ_AUTHENTICATE = 200;
    protected AccountManager mAccountManager;
    protected Account mAccount;

    /**
     * Instance of the operations ("BaseOps") type.
     */
    private BaseOps mOpsInstance;

    public void onCreate(Bundle savedInstanceState,
                         Class<BaseOps> opsType) {

        super.onCreate(savedInstanceState, opsType);

        mAccountManager = AccountManager.get(getBaseContext());

        if (setAccount()){
            runDataSync(mAccount);
        }
        else {
            requestAuthentication();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gotit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_feedback) {
            Intent intent = new Intent(this, ChartActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_checkin) {
            Intent intent = new Intent(this, CheckInListActivity.class);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_followers) {
            Intent intent = new Intent(this, FollowerActivity.class);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_following) {
            Intent intent = new Intent(this, FollowingActivity.class);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_login) {
            requestAuthentication();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean setAccount(){
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (sCurrentAccountName != null) {
            for (Account account : accounts){
                if (account.name.equals(sCurrentAccountName)){
                    mAccount = account;
                    return true;
                }
            }
        }
        else {
            if(accounts.length > 0) {
                mAccount = accounts[0];
                return true;
            }
        }
        return false;
    }

    public void requestAuthentication(){
        Intent intent = new Intent(getBaseContext(),
                LoginActivity.class);

        intent.putExtra(ARG_ACCOUNT_TYPE, ACCOUNT_TYPE);
        intent.putExtra(ARG_IS_ADDING_NEW_ACCOUNT, true);

        startActivityForResult(intent, REQ_AUTHENTICATE );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_AUTHENTICATE && resultCode == RESULT_OK) {

            sCurrentAccountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

            if(setAccount())
                runDataSync(mAccount);

        } else
            super.onActivityResult(requestCode, resultCode, data);
    }


    public void runDataSync(Account account){

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        ContentResolver.requestSync(account,
                getApplicationContext()
                        .getString(R.string.content_authority),
                bundle);
    }
}
