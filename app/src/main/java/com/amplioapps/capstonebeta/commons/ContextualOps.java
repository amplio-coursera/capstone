package com.amplioapps.capstonebeta.commons;

import android.app.Activity;
import android.content.Context;

/**
 * The base interface that an operations ("Ops") class must implement
 * so that it commands are able to get the application context.
 */
public interface ContextualOps {

    public Context getApplicationContext();

    public Activity getActivity();
}
