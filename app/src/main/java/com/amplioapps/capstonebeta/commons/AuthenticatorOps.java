package com.amplioapps.capstonebeta.commons;


import android.app.Activity;
import android.content.Intent;

public interface AuthenticatorOps {

    void onConfiguration(Activity activity,
                         boolean firstTimeIn);

    void onUserLogin(String... params);

    void onUserRegister();

    void onSignupResult(Intent data);

}
