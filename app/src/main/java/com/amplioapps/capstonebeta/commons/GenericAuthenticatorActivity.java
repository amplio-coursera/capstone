package com.amplioapps.capstonebeta.commons;


import android.accounts.AccountAuthenticatorActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class GenericAuthenticatorActivity <OpsType extends AuthenticatorOps >
    extends AccountAuthenticatorActivity {

    protected final String TAG = getClass().getSimpleName();

    private final RetainedFragmentManager mRetainedFragmentManager
            = new RetainedFragmentManager(this.getFragmentManager(),
            TAG);

    private final int REQ_SIGNUP = 1;

    private OpsType mOpsInstance;

    public void onCreate(Bundle savedInstanceState,
                         Class<OpsType> opsType) {
        // Call up to the super class.
        super.onCreate(savedInstanceState);

        try {
            // Handle configuration-related events, including the
            // initial creation of an Activity and any subsequent
            // runtime configuration changes.
            handleConfiguration(opsType);
        } catch (InstantiationException
                | IllegalAccessException e) {
            Log.d(TAG,
                    "handleConfiguration "
                            + e);
            // Propagate this as a runtime exception.
            throw new RuntimeException(e);
        }
    }

    public void handleConfiguration(Class<OpsType> opsType)
            throws InstantiationException, IllegalAccessException {

        // If this method returns true it's the first time the
        // Activity has been created.
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(TAG,
                    "First time onCreate() call");

            // Initialize the GenericActivity fields.
            initialize(opsType);
        } else {
            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change
            // occured.
            Log.d(TAG,
                    "Second or subsequent onCreate() call");

            // Try to obtain the OpsType instance from the
            // RetainedFragmentManager.
            mOpsInstance =
                    mRetainedFragmentManager.get(opsType.getSimpleName());

            // This check shouldn't be necessary under normal
            // circumstances, but it's better to lose state than to
            // crash!
            if (mOpsInstance == null)
                // Initialize the GenericActivity fields.
                initialize(opsType);
            else
                // Inform it that the runtime configuration change has
                // completed.
                mOpsInstance.onConfiguration(this,
                        false);
        }
    }

    private void initialize(Class<OpsType> opsType)
            throws InstantiationException, IllegalAccessException {
        // Create the OpsType object.
        mOpsInstance = opsType.newInstance();

        // Put the OpsInstance into the RetainedFragmentManager under
        // the simple name.
        mRetainedFragmentManager.put(opsType.getSimpleName(),
                mOpsInstance);

        // Perform the first initialization.
        mOpsInstance.onConfiguration(this,
                true);
    }

    public void startSignupActivity(Intent intent){
        startActivityForResult(intent, REQ_SIGNUP);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_SIGNUP && resultCode == RESULT_OK) {
            mOpsInstance.onSignupResult(data);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    public OpsType getOps() {
        return mOpsInstance;
    }

    public RetainedFragmentManager getRetainedFragmentManager() {
        return mRetainedFragmentManager;
    }

}
