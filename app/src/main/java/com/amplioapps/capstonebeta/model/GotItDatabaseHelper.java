package com.amplioapps.capstonebeta.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;


public class GotItDatabaseHelper extends SQLiteOpenHelper {

    /**
     * Database name.
     */
    private static String DATABASE_NAME =
            "com_amplioapp_gotitclient_db";

    /**
     * Database version number, which is updated with each schema
     * change.
     */
    private static int DATABASE_VERSION = 1;


    final String SQL_CREATE_CHECKIN_TABLE =
            "CREATE TABLE "
                    + CheckInContract.CheckInEntry.TABLE_NAME + " ("
                    + CheckInContract.CheckInEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.CheckInEntry.COLUMN_OWNER + " INTEGER NOT NULL, "
                    + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP+ " TEXT NOT NULL "
                    + " );";

    final String SQL_CREATE_ANSWER_TABLE =
            "CREATE TABLE "
                    + CheckInContract.AnswerEntry.TABLE_NAME + " ("
                    + CheckInContract.AnswerEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.AnswerEntry.COLUMN_QUESTIONID + " INTEGER NOT NULL, "
                    + CheckInContract.AnswerEntry.COLUMN_CHECKINID + " INTEGER NOT NULL, "
                    + CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT + " TEXT NOT NULL "
                    + " );";

    final String SQL_CREATE_QUESTION_TABLE =
            "CREATE TABLE "
                    + CheckInContract.QuestionEntry.TABLE_NAME + " ("
                    + CheckInContract.QuestionEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.QuestionEntry.COLUMN_QUESTIONID + " INTEGER NOT NULL, "
                    + CheckInContract.QuestionEntry.COLUMN_QUESTIONTYPE + " TEXT NOT NULL, "
                    + CheckInContract.QuestionEntry.COLUMN_QUESTIONTEXT + " TEXT NOT NULL "
                    + " );";

    final String SQL_CREATE_USERS_TABLE =
            "CREATE TABLE "
                    + CheckInContract.UsersEntry.TABLE_NAME + " ("
                    + CheckInContract.UsersEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.UsersEntry.COLUMN_USERID + " INTEGER NOT NULL, "
                    + CheckInContract.UsersEntry.COLUMN_USERNAME + " TEXT NOT NULL "
                    + " );";

    final String SQL_CREATE_FOLLOWER_TABLE =
            "CREATE TABLE "
                    + CheckInContract.FollowerEntry.TABLE_NAME + " ("
                    + CheckInContract.FollowerEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.FollowerEntry.COLUMN_OWNERID + " INTEGER NOT NULL, "
                    + CheckInContract.FollowerEntry.COLUMN_FOLLOWERID + " INTEGER NOT NULL "
                    + " );";

    final String SQL_CREATE_FOLLOWING_TABLE =
            "CREATE TABLE "
                    + CheckInContract.FollowingEntry.TABLE_NAME + " ("
                    + CheckInContract.FollowingEntry._ID + " INTEGER PRIMARY KEY, "
                    + CheckInContract.FollowingEntry.COLUMN_OWNERID + " INTEGER NOT NULL, "
                    + CheckInContract.FollowingEntry.COLUMN_FOLLOWINGID + " TEXT NOT NULL "
                    + " );";

    /**
     * Constructor - initialize database name and version, but don't
     * actually construct the database (which is done in the
     * onCreate() hook method). It places the database in the
     * application's cache directory, which will be automatically
     * cleaned up by Android if the device runs low on storage space.
     *
     * @param context
     */
    public GotItDatabaseHelper(Context context) {
        super(context,
                context.getCacheDir()
                        + File.separator
                        + DATABASE_NAME,
                null,
                DATABASE_VERSION);
    }


    /**
     * Hook method called when the database is created.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create the table.
        db.execSQL(SQL_CREATE_CHECKIN_TABLE);
        db.execSQL(SQL_CREATE_ANSWER_TABLE);
        db.execSQL(SQL_CREATE_QUESTION_TABLE);
        db.execSQL(SQL_CREATE_USERS_TABLE);
        db.execSQL(SQL_CREATE_FOLLOWER_TABLE);
        db.execSQL(SQL_CREATE_FOLLOWING_TABLE);
    }

    /**
     * Hook method called when the database is upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db,
                          int oldVersion,
                          int newVersion) {
        // Delete the existing tables.
        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.CheckInEntry.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.AnswerEntry.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.QuestionEntry.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.UsersEntry.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.FollowerEntry.TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "
                + CheckInContract.FollowingEntry.TABLE_NAME);

        // Create the new tables.
        onCreate(db);
    }
}

