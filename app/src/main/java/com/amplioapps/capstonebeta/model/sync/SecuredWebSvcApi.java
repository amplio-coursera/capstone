package com.amplioapps.capstonebeta.model.sync;

import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.model.CheckInQuestion;
import com.amplioapps.capstonebeta.model.FollowsRelation;
import com.amplioapps.capstonebeta.model.SimpleUser;
import com.amplioapps.capstonebeta.model.auth.SignupAccountDetails;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface SecuredWebSvcApi {

    public static final String QBANK_SVC_PATH = "/qbank";
    public static final String USER_SVC_PATH = "/user";
    public static final String USERS_LIST_SVC_PATH = "/users";
    public static final String CHECKINS_SVC_PATH = "/checkins";
    public static final String FOLLOWERS_SVC_PATH = "/followers";
    public static final String FOLLOWING_SVC_PATH = "/following";

    @GET(QBANK_SVC_PATH)
    public Collection<CheckInQuestion> getQuestionsList();

    @GET(USER_SVC_PATH)
    public SignupAccountDetails getUserObject();

    @GET(USERS_LIST_SVC_PATH)
    public Collection<SimpleUser> getUsersList();

    @GET(CHECKINS_SVC_PATH)
    public Collection<CheckInItem> getCheckInList();

    @POST(CHECKINS_SVC_PATH)
    public int addCheckIns(@Body Collection<CheckInItem> c);

    @GET(FOLLOWERS_SVC_PATH)
    public Collection<FollowsRelation> getFollowerList();

    @POST(FOLLOWERS_SVC_PATH)
    public int addFollowerList(@Body Collection<FollowsRelation> u);

}
