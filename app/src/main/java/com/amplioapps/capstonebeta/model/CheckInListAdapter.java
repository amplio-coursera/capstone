package com.amplioapps.capstonebeta.model;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.view.CheckInDetailActivity;
import com.amplioapps.capstonebeta.view.FollowListActivity;

import java.text.SimpleDateFormat;
import java.util.Date;


public class CheckInListAdapter extends SimpleCursorAdapter {

    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;

    private ContentResolver mCr;

    public CheckInListAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layout = layout;
        this.mContext = context;
        this.inflater=LayoutInflater.from(context);
        this.cr=c;
        mCr = mContext.getContentResolver();
    }

    @Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);

        TextView tvId=(TextView)view.findViewById(R.id.label_id);
        TextView tvOwner=(TextView)view.findViewById(R.id.owner);
        TextView tvTimestamp=(TextView)view.findViewById(R.id.timestamp);

        Button btItem = (Button) view.findViewById(R.id.buttonView);

        int indexId=cursor.getColumnIndexOrThrow(
                CheckInContract.CheckInEntry._ID);
        int indexUser=cursor.getColumnIndexOrThrow(
                CheckInContract.UsersEntry.COLUMN_USERNAME);
        int indexTimestamp=cursor.getColumnIndexOrThrow(
                CheckInContract.CheckInEntry.COLUMN_TIMESTAMP);

        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date timestamp = new Date(cursor.getLong(indexTimestamp));
        tvTimestamp.setText(df2.format(timestamp));

        tvOwner.setText(cursor.getString(indexUser));

        tvId.setText(cursor.getString(indexId));

        final String checkInId = cursor.getString(indexId);

        btItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CheckInDetailActivity.class);
                intent.putExtra(CheckInDetailActivity.ARG_CHECKIN_DETAIL_ID,
                        checkInId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }
}
