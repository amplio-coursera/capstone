package com.amplioapps.capstonebeta.model.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class GotItSyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();
    private static GotItSyncAdapter sGotItSyncAdapter = null;

    @Override
    public void onCreate() {
        Log.d("GotItSyncService", "onCreate - GotItSyncService");
        synchronized (sSyncAdapterLock) {
            if (sGotItSyncAdapter == null) {
                sGotItSyncAdapter = new GotItSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sGotItSyncAdapter.getSyncAdapterBinder();
    }

}