package com.amplioapps.capstonebeta.model;


import android.database.Cursor;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;

public class FollowsRelation {

    long ownerid;
    long otherid;

    public FollowsRelation(){
    }

    public FollowsRelation(long ownerid, long otherid) {
        this.ownerid = ownerid;
        this.otherid = otherid;
    }

    public long getOwnerid(){
        return ownerid;
    }
    public void setOwnerid(long ownerid){
        this.ownerid = ownerid;
    }
    public long getOtherid(){
        return ownerid;
    }
    public void setOtherid(long otherid){
        this.otherid = otherid;
    }

    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(ownerid, otherid);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FollowsRelation) {
            FollowsRelation other = (FollowsRelation) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(ownerid, other.ownerid)
                    && Objects.equal(otherid, other.otherid);
        } else {
            return false;
        }
    }

}
