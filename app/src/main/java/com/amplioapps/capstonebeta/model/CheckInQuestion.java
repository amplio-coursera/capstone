package com.amplioapps.capstonebeta.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;

public class CheckInQuestion {

    private long questionId;

    private String questionText;

    private String responseType;


    public CheckInQuestion() {
    }

    public CheckInQuestion(long questionId, String questionText, String responseType) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.responseType = responseType;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(questionText, responseType);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CheckInQuestion) {
            CheckInQuestion other = (CheckInQuestion) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(questionText, other.questionText)
                    && Objects.equal(responseType, other.responseType);
        } else {
            return false;
        }
    }

    public ContentValues getQuestionCvs(){
        ContentValues cvs = new ContentValues();
        cvs.put(CheckInContract.QuestionEntry.COLUMN_QUESTIONID, getQuestionId());
        cvs.put(CheckInContract.QuestionEntry.COLUMN_QUESTIONTYPE, getResponseType());
        cvs.put(CheckInContract.QuestionEntry.COLUMN_QUESTIONTEXT, getQuestionText());

        return cvs;
    }

    public static Collection<CheckInQuestion> fromCursor(Cursor cursor) {
        ArrayList<CheckInQuestion> questions =
                new ArrayList<CheckInQuestion>();

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndexOrThrow(
                    CheckInContract.QuestionEntry.COLUMN_QUESTIONID);
            int idType = cursor.getColumnIndexOrThrow(
                    CheckInContract.QuestionEntry.COLUMN_QUESTIONTYPE);
            int idText = cursor.getColumnIndexOrThrow(
                    CheckInContract.QuestionEntry.COLUMN_QUESTIONTEXT);

            while (!cursor.isAfterLast()) {
                questions.add(
                        new CheckInQuestion(
                                cursor.getLong(idIndex),
                                cursor.getString(idText),
                                cursor.getString(idType)));

                cursor.moveToNext();
            }
            cursor.close();
        }
        return questions;
    }

}
