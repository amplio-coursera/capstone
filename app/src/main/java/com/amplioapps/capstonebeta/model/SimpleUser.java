package com.amplioapps.capstonebeta.model;


import android.content.ContentValues;
import android.database.Cursor;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;

public class SimpleUser {

    private long id;
    private String username;

    public SimpleUser() {
    }

    public SimpleUser(long id, String username) {
        this.id = id;
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SimpleUser) {
            SimpleUser other = (SimpleUser) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(id, other.id)
                    && Objects.equal(username, other.username);

        } else {
            return false;
        }
    }

    public ContentValues getUserCvs(){
        ContentValues cvs = new ContentValues();
        cvs.put(CheckInContract.UsersEntry.COLUMN_USERID, getId());
        cvs.put(CheckInContract.UsersEntry.COLUMN_USERNAME, getUsername());

        return cvs;
    }

    public static Collection<SimpleUser> fromCursor(Cursor cursor) {
        ArrayList<SimpleUser> users =
                new ArrayList<SimpleUser>();

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndexOrThrow(
                    CheckInContract.UsersEntry.COLUMN_USERID);
            int idName = cursor.getColumnIndexOrThrow(
                    CheckInContract.UsersEntry.COLUMN_USERNAME);

            while (!cursor.isAfterLast()) {
                users.add(
                        new SimpleUser(
                                cursor.getLong(idIndex),
                                cursor.getString(idName)));

                cursor.moveToNext();
            }
            cursor.close();
        }
        return users;
    }
}
