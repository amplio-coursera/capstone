package com.amplioapps.capstonebeta.model.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.amplioapps.capstonebeta.model.auth.GotItAuthenticator;

public class GotItAutheticationService extends Service{
    // Instance field that stores the authenticator object
    private GotItAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new GotItAuthenticator(this);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}