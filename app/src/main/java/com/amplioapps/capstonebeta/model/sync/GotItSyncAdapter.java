package com.amplioapps.capstonebeta.model.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.model.CheckInContract;
import com.amplioapps.capstonebeta.model.CheckInItem;
import com.amplioapps.capstonebeta.model.CheckInQuestion;
import com.amplioapps.capstonebeta.model.FollowsRelation;
import com.amplioapps.capstonebeta.model.SimpleUser;
import com.amplioapps.capstonebeta.model.auth.WebSvcConfig;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

import static com.amplioapps.capstonebeta.model.GotItProviderImpl.sUriMatcher;


public class GotItSyncAdapter extends AbstractThreadedSyncAdapter {

    public final String TAG = GotItSyncAdapter.class.getSimpleName();

    private final Client mClient = new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient());

    private TokenInterceptor mInterceptor;

    private SecuredWebSvcApi mWebSvcApi;

    private AccountManager mAccountManager;

    public GotItSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mAccountManager = AccountManager.get(context);
    }

    @Override
    public void onPerformSync(Account account,
                              Bundle extras,
                              String authority,
                              ContentProviderClient provider,
                              SyncResult syncResult) {

        Log.d(TAG, "onPerformSync Called.");

        // Building a print of the extras we got
        StringBuilder sb = new StringBuilder();
        if (extras != null) {
            for (String key : extras.keySet()) {
                sb.append(key + "[" + extras.get(key) + "] ");
            }
        }

        Log.d(TAG, "> onPerformSync for account[" + account.name + "]. Extras: " + sb.toString());

        try {
            // Get the auth token for the current account
            String authToken =
                    mAccountManager.blockingGetAuthToken(
                            account,
                            WebSvcConfig.TOKEN_TYPE_FULL,
                            true);

            mInterceptor = new TokenInterceptor(authToken);

            mWebSvcApi = new RestAdapter.Builder()
                    .setClient(mClient)
                    .setEndpoint(WebSvcConfig.TEST_URL)
                    .setRequestInterceptor(mInterceptor) // Register Oauth interceptor
                    .setLogLevel(RestAdapter.LogLevel.FULL).build()
                    .create(SecuredWebSvcApi.class);


            // Get remote questions
            Collection<CheckInQuestion> remoteQuestions =
                    mWebSvcApi.getQuestionsList();

            // Get local questions
            Cursor curQuestions = provider.query(
                    CheckInContract.QuestionEntry.CONTENT_URI, null, null, null, null);
            Collection<CheckInQuestion> localQuestions =
                    CheckInQuestion.fromCursor(curQuestions);

            // Collection<CheckInQuestion> questionsToRemote =
            //        getQuestionDelta(localQuestions, remoteQuestions);

            /** One Way Sync Only for questions **/
            Collection<CheckInQuestion> questionsToLocal =
                    getQuestionDelta(remoteQuestions, localQuestions);

            if (questionsToLocal.size() == 0) {
                Log.d(TAG, "> No server changes to update local database");
            } else {
                Log.d(TAG, "> Updating local database with remote changes");

                provider.bulkInsert(CheckInContract.QuestionEntry.CONTENT_URI,
                        getQuestionValues(questionsToLocal));
            }


            // Get remote users
            Collection<SimpleUser> remoteUsers =
                    mWebSvcApi.getUsersList();

            // Get local questions
            Cursor curUsers = provider.query(
                    CheckInContract.UsersEntry.CONTENT_URI, null, null, null, null);
            Collection<SimpleUser> localUsers =
                    SimpleUser.fromCursor(curUsers);

            /** One Way Sync Only for users **/
            Collection<SimpleUser> usersToLocal =
                    getUserDelta(remoteUsers, localUsers);


            if (usersToLocal.size() == 0) {
                Log.d(TAG, "> No server changes to update local database");
            } else {
                Log.d(TAG, "> Updating local database with remote changes");

                provider.bulkInsert(CheckInContract.UsersEntry.CONTENT_URI,
                        getUserValues(usersToLocal));
            }

            // Get remote checkins
            Collection<CheckInItem> remoteCheckIns =
                    mWebSvcApi.getCheckInList();

            // Get local checkins
            Cursor curCheckIns = provider.query(
                    CheckInContract.CHECKIN_ANSWER_URI, null, null, null, null);
            Collection<CheckInItem> localCheckIns =
                    CheckInItem.fromCursor(curCheckIns);

            Collection<CheckInItem> checkInsToLocal =
                    getCheckInDelta(remoteCheckIns, localCheckIns);

            Collection<CheckInItem> checkInsToRemote =
                    getCheckInDelta(localCheckIns, remoteCheckIns);

            if (checkInsToLocal.size() == 0) {
                Log.d(TAG, "> No server changes to update local database");
            } else {
                Log.d(TAG, "> Updating local database with remote changes");

                provider.bulkInsert(CheckInContract.CheckInEntry.CONTENT_URI,
                        getCheckInValues(checkInsToLocal));

                provider.bulkInsert(CheckInContract.AnswerEntry.CONTENT_URI,
                        getAnswerValues(checkInsToLocal));
            }

            if (checkInsToRemote.size() == 0) {
                Log.d(TAG, "> No local changes to update server database");
            } else {
                Log.d(TAG, "> Updating server database with local changes");

                mWebSvcApi.addCheckIns(checkInsToRemote);
            }

/*
            // Get remote followers
            Collection<FollowsRelation> remoteFollowers =
                    mWebSvcApi.getFollowerList();

            // Get local questions
            Cursor curFollowers = provider.query(
                    CheckInContract.FollowerEntry.CONTENT_URI, null, null, null, null);
            Collection<FollowsRelation> localFollowers =
                    getFollowerRelation(curFollowers);

            Collection<FollowsRelation> followersToLocal =
                    getFollowerDelta(remoteFollowers, localFollowers);

            Collection<FollowsRelation> followersToRemote =
                    getFollowerDelta(localFollowers, remoteFollowers);

            if (followersToLocal.size() == 0) {
                Log.d(TAG, "> No server changes to update local database");
            } else {
                Log.d(TAG, "> Updating local database with remote changes");

                provider.bulkInsert(CheckInContract.FollowerEntry.CONTENT_URI,
                        getFollowerValues(followersToLocal));
            }

            if (followersToRemote.size() == 0) {
                Log.d(TAG, "> No local changes to update server database");
            } else {
                Log.d(TAG, "> Updating server database with local changes");

                mWebSvcApi.addFollowerList(followersToRemote);
            }
*/

        } catch (OperationCanceledException e) {
            e.printStackTrace();
        } catch (IOException e) {
            syncResult.stats.numIoExceptions++;
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            syncResult.stats.numAuthExceptions++;
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Collection<CheckInQuestion> getQuestionDelta (
            Collection<CheckInQuestion> syncFrom,
            Collection<CheckInQuestion> syncTo) {

        ArrayList<CheckInQuestion> questionsTo = new ArrayList<CheckInQuestion>();

        for (CheckInQuestion question : syncFrom) {
            if (!syncTo.contains(question))
                questionsTo.add(question);
        }

        return questionsTo;
    }

    private ContentValues[] getQuestionValues(Collection<CheckInQuestion> questions) {
        ContentValues questionsToLocalValues[] = new ContentValues[questions.size()];
        int i = 0;
        for (CheckInQuestion localQuestion : questions) {
            Log.d(TAG, "> Remote -> Local [" + localQuestion.getQuestionId() + "]");
            questionsToLocalValues[i++] = localQuestion.getQuestionCvs();
        }
        return questionsToLocalValues;
    }

    private Collection<SimpleUser> getUserDelta (
            Collection<SimpleUser> syncFrom,
            Collection<SimpleUser> syncTo) {

        ArrayList<SimpleUser> usersTo = new ArrayList<SimpleUser>();

        for (SimpleUser user : syncFrom) {
            if (!syncTo.contains(user))
                usersTo.add(user);
        }

        return usersTo;
    }

    private ContentValues[] getUserValues(Collection<SimpleUser> users) {
        ContentValues usersToLocalValues[] = new ContentValues[users.size()];
        int i = 0;
        for (SimpleUser localUser : users) {
            Log.d(TAG, "> Remote -> Local [" + localUser.getUsername() + "]");
            usersToLocalValues[i++] = localUser.getUserCvs();
        }
        return usersToLocalValues;
    }

    private Collection<CheckInItem> getCheckInDelta (
            Collection<CheckInItem> syncFrom,
            Collection<CheckInItem> syncTo) {

        ArrayList<CheckInItem> checkInTo = new ArrayList<CheckInItem>();

        for (CheckInItem item : syncFrom) {
            if (!syncTo.contains(item))
                checkInTo.add(item);
        }

        return checkInTo;
    }

    private ContentValues[] getCheckInValues(Collection<CheckInItem> checkins) {
        ContentValues checkInsToLocalValues[] = new ContentValues[checkins.size()];
        int i = 0;
        for (CheckInItem localCheckIn : checkins) {
            Log.d(TAG, "> Remote -> Local Checkins [" + localCheckIn.getResponseId() + "]");
            checkInsToLocalValues[i++] = localCheckIn.getCheckinCvs();
        }
        return checkInsToLocalValues;
    }

    private ContentValues[] getAnswerValues(Collection<CheckInItem> checkins) {
        ArrayList<ContentValues> cvsArray = new ArrayList<ContentValues>();
        int i = 0;
        for (CheckInItem localCheckIn : checkins) {
            Log.d(TAG, "> Remote -> Local Answers [" + localCheckIn.getResponseId() + "]");
            cvsArray.addAll(
                    new ArrayList<ContentValues>(
                            Arrays.asList(localCheckIn.getAnswersCvs())));
        }
        return cvsArray.toArray(new ContentValues[cvsArray.size()]);
    }

    private Collection<FollowsRelation> getFollowerRelation(Cursor cursor) {
        ArrayList<FollowsRelation> relation =
                new ArrayList<FollowsRelation>();

        if (cursor.moveToFirst()) {
            int idOwner = cursor.getColumnIndexOrThrow(
                    CheckInContract.FollowerEntry.COLUMN_OWNERID);
            int idFollower = cursor.getColumnIndexOrThrow(
                    CheckInContract.FollowerEntry.COLUMN_FOLLOWERID);

            while (!cursor.isAfterLast()) {
                relation.add(
                        new FollowsRelation(
                                cursor.getLong(idOwner),
                                cursor.getLong(idFollower)));

                cursor.moveToNext();
            }
            cursor.close();
        }
        return relation;
    }

    private Collection<FollowsRelation> getFollowerDelta (
            Collection<FollowsRelation> syncFrom,
            Collection<FollowsRelation> syncTo) {

        ArrayList<FollowsRelation> followersTo = new ArrayList<FollowsRelation>();

        for (FollowsRelation item : syncFrom) {
            if (!syncTo.contains(item))
                followersTo.add(item);
        }

        return followersTo;
    }

    private ContentValues[] getFollowerValues(Collection<FollowsRelation> relations) {
        ContentValues followersToLocalValues[] = new ContentValues[relations.size()];
        int i = 0;
        for (FollowsRelation localFollower : relations) {
            Log.d(TAG, "> Remote -> Local Follower [" + localFollower.getOwnerid() + "]");
            followersToLocalValues[i++] = getFollowerCvs(localFollower);
        }
        return followersToLocalValues;
    }

    private ContentValues getFollowerCvs(FollowsRelation follower) {
        ContentValues cvs = new ContentValues();
        cvs.put(CheckInContract.FollowerEntry.COLUMN_OWNERID, follower.getOwnerid());
        cvs.put(CheckInContract.FollowerEntry.COLUMN_FOLLOWERID, follower.getOwnerid());
        return cvs;
    }

}