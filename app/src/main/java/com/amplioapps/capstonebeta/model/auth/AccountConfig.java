package com.amplioapps.capstonebeta.model.auth;

public class AccountConfig {

    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";
    public static final String PARAM_USER_PASS = "USER_PASS";

    public static final String ACCOUNT_TYPE = "com.amplioapps.gotitwebsvc";

    public static final String ACCOUNT_NAME = "gotituser";

    public static final String USERDATA_USER_OBJ_ID = "GotItUserId";

    public static String sCurrentAccountName;

}
