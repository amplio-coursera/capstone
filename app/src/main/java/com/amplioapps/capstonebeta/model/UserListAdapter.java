package com.amplioapps.capstonebeta.model;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amplioapps.capstonebeta.R;
import com.amplioapps.capstonebeta.view.FollowListActivity;
import com.amplioapps.capstonebeta.view.FollowerActivity;


public class UserListAdapter extends SimpleCursorAdapter {
    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;

    public static final int FOLLOWER_LIST_TYPE = 1;
    public static final int FOLLOWING_LIST_TYPE = 2;
    private int type;
    private String userId;
    private ContentResolver mCr;

    public UserListAdapter(int type, Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layout = layout;
        this.mContext = context;
        this.inflater=LayoutInflater.from(context);
        this.cr=c;
        this.type = type;
    }

    @Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);

        TextView tvId=(TextView)view.findViewById(R.id.userId);
        TextView tvUsername=(TextView)view.findViewById(R.id.username);
        Button btItem = (Button) view.findViewById(R.id.buttonView);

        int indexId=cursor.getColumnIndexOrThrow(
                CheckInContract.FollowerEntry._ID);
        int indexUserId=cursor.getColumnIndexOrThrow(
                CheckInContract.UsersEntry.COLUMN_USERID);
        int indexUsername=cursor.getColumnIndexOrThrow(
                CheckInContract.UsersEntry.COLUMN_USERNAME);

        userId = cursor.getString(indexUserId);

        tvId.setText(cursor.getString(indexUserId));

        tvUsername.setText(cursor.getString(indexUsername));

        switch (type) {
            case FOLLOWER_LIST_TYPE:
                btItem.setText("Remove");

                int indexOwner=cursor.getColumnIndexOrThrow(
                        CheckInContract.FollowerEntry.COLUMN_OWNERID);
                final String ownerId = cursor.getString(indexOwner);

                btItem.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        mCr = mContext.getContentResolver();
                        mCr.delete(CheckInContract.FollowerEntry.CONTENT_URI,
                                CheckInContract.FollowerEntry.COLUMN_OWNERID
                                        + "=? AND "
                                        + CheckInContract.FollowerEntry.COLUMN_FOLLOWERID
                                        + "=?",
                                new String[] { ownerId, userId });
                        mCr.delete(CheckInContract.FollowingEntry.CONTENT_URI,
                                CheckInContract.FollowingEntry.COLUMN_OWNERID
                                        + "=? AND "
                                        + CheckInContract.FollowingEntry.COLUMN_FOLLOWINGID
                                        + "=?",
                                new String[] { userId, ownerId });
                    }
                });

                break;
            case FOLLOWING_LIST_TYPE:
                btItem.setText("View");
                btItem.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, FollowListActivity.class);
                        intent.putExtra(FollowListActivity.ARG_FOLLOWING_USER,
                                userId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                });
                break;
        }
    }
}
