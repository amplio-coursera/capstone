package com.amplioapps.capstonebeta.model.auth;

import retrofit.http.Body;
import retrofit.http.POST;;

public interface SignupSvcApi {

    public static final String SIGNUP_SVC_PATH = "/signup";

    @POST(SIGNUP_SVC_PATH)
    public SignupAccountDetails addUserAccount(@Body SignupAccountDetails d);

}
