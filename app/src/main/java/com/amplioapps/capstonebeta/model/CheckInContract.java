package com.amplioapps.capstonebeta.model;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import com.amplioapps.capstonebeta.R;

public class CheckInContract {

    /**
     * This ContentProvider's unique identifier.
     */
    public static final String CONTENT_AUTHORITY =
            "com.amplioapps.gotitprovider";

    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which
     * apps will use to contact the content provider.
     */
    public static final Uri BASE_CONTENT_URI =
            Uri.parse("content://"
                    + CONTENT_AUTHORITY);

    /**
     * Possible paths (appended to base content URI for possible
     * URI's).  For instance, content://vandy.mooc/character_table/ is
     * a valid path for looking at Character data.  Conversely,
     * content://vandy.mooc/givemeroot/ will fail, as the
     * ContentProvider hasn't been given any information on what to do
     * with "givemeroot".
     */
    public static final String PATH_CHECKIN =
            CheckInEntry.TABLE_NAME;

    public static final String PATH_ANSWER =
            AnswerEntry.TABLE_NAME;

    public static final String PATH_QUESTION =
            QuestionEntry.TABLE_NAME;

    public static final String PATH_USERS =
            UsersEntry.TABLE_NAME;

    public static final String PATH_FOLLOWER =
            FollowerEntry.TABLE_NAME;

    public static final String PATH_FOLLOWING =
            FollowingEntry.TABLE_NAME;

    /**
     *  Paths and URIs for Joined Data Sets
     */

    public static final String PATH_CHECKIN_ANSWER =
            "combined_checkin_and_answer";

    public static final Uri CHECKIN_ANSWER_URI =
            BASE_CONTENT_URI.buildUpon().appendPath
                    (PATH_CHECKIN_ANSWER).build();

    public static final String CHECKIN_ANSWER_TYPE =
            "vnd.android.cursor.dir/"
                    + CONTENT_AUTHORITY
                    + "/"
                    + PATH_CHECKIN_ANSWER;


    public static final String PATH_CHECKIN_USER =
            "combined_checkin_and_user";

    public static final Uri CHECKIN_USER_URI =
            BASE_CONTENT_URI.buildUpon().appendPath
                    (PATH_CHECKIN_USER).build();

    public static final String CHECKIN_USER_TYPE =
            "vnd.android.cursor.dir/"
                    + CONTENT_AUTHORITY
                    + "/"
                    + PATH_CHECKIN_USER;


    public static final String PATH_FOLLOWER_USER =
            "combined_follower_and_user";

    public static final Uri FOLLOWER_USER_URI =
            BASE_CONTENT_URI.buildUpon().appendPath
                    (PATH_FOLLOWER_USER).build();

    public static final String FOLLOWER_USER_TYPE =
            "vnd.android.cursor.dir/"
                    + CONTENT_AUTHORITY
                    + "/"
                    + PATH_FOLLOWER_USER;

    /**
     * Columns to display.
     */
    public static final String sFollowerUserColumnsToDisplay [] =
            new String[] {
                    UsersEntry.COLUMN_USERID,
                    UsersEntry.COLUMN_USERNAME
            };

    /**
     * Resource Ids of the columns to display.
     */

    public static final int[] sFollowerUserColumnResIds =
            new int[] {
                    R.id.userId,
                    R.id.username
            };


    public static final String PATH_FOLLOWING_USER =
            "combined_following_and_user";

    public static final Uri FOLLOWING_USER_URI =
            BASE_CONTENT_URI.buildUpon().appendPath
                    (PATH_FOLLOWING_USER).build();

    public static final String FOLLOWING_USER_TYPE =
            "vnd.android.cursor.dir/"
                    + CONTENT_AUTHORITY
                    + "/"
                    + PATH_FOLLOWING_USER;


    public static final class FollowingEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_FOLLOWING).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_FOLLOWING;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_FOLLOWING;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        FollowingEntry.COLUMN_OWNERID,
                        FollowingEntry.COLUMN_FOLLOWINGID
                };

        /**
         * Resource Ids of the columns to display.
         */

        public static final int[] sColumnResIds =
                new int[] {
                        R.id.owner,
                        R.id.timestamp
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "following_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_OWNERID = "ownerid";
        public static final String COLUMN_FOLLOWINGID = "followingid";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }

    public static final class FollowerEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_FOLLOWER).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_FOLLOWER;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_FOLLOWER;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        FollowerEntry.COLUMN_OWNERID,
                        FollowerEntry.COLUMN_FOLLOWERID
                };

        /**
         * Resource Ids of the columns to display.
         */

        public static final int[] sColumnResIds =
                new int[] {
                        R.id.owner,
                        R.id.timestamp
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "follower_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_OWNERID = "ownerid";
        public static final String COLUMN_FOLLOWERID = "followerid";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }

    public static final class UsersEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_USERS).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_USERS;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_USERS;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        UsersEntry.COLUMN_USERID,
                        UsersEntry.COLUMN_USERNAME
                };

        /**
         * Resource Ids of the columns to display.
         */

        public static final int[] sColumnResIds =
                new int[] {
                        R.id.owner,
                        R.id.timestamp
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "users_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_USERID = "userid";
        public static final String COLUMN_USERNAME = "username";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }


    public static final class CheckInEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_CHECKIN).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_CHECKIN;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_CHECKIN;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        CheckInContract.CheckInEntry._ID,
                        CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                };

        /**
         * Resource Ids of the columns to display.
         */

        public static final int[] sColumnResIds =
                new int[] {
                        R.id.owner,
                        R.id.timestamp
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "checkin_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_OWNER = "owner";
        public static final String COLUMN_TIMESTAMP = "timestamp";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }


    public static final class AnswerEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_ANSWER).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_ANSWER;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_ANSWER;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        CheckInContract.AnswerEntry._ID,
                        CheckInContract.AnswerEntry.COLUMN_QUESTIONID,
                        CheckInContract.AnswerEntry.COLUMN_CHECKINID,
                        CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT
                };

        /**
         * Resource Ids of the columns to display.
         */
/*
        public static final int[] sColumnResIds =
                new int[] {
                        R.id.idString,
                        R.id.name,
                        R.id.race
                };
*/
        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "answer_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_QUESTIONID = "questionid";
        public static final String COLUMN_CHECKINID = "checkinid";
        public static final String COLUMN_ANSWERTEXT = "answertext";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }


    public static final class QuestionEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_QUESTION).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_QUESTION;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_QUESTION;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        CheckInContract.QuestionEntry._ID,
                        CheckInContract.QuestionEntry.COLUMN_QUESTIONTYPE,
                        CheckInContract.QuestionEntry.COLUMN_QUESTIONTEXT
                };

        /**
         * Resource Ids of the columns to display.
         */
/*
        public static final int[] sColumnResIds =
                new int[] {
                        R.id.idString,
                        R.id.qtext,
                        R.id.qtype
                };
*/

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "question_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_QUESTIONID = "questionid";
        public static final String COLUMN_QUESTIONTYPE = "questiontype";
        public static final String COLUMN_QUESTIONTEXT = "questiontext";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }

}
