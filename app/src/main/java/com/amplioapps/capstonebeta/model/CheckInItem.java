package com.amplioapps.capstonebeta.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class CheckInItem {

    private long responseId;

    private long timestamp;

    private Map<Long,String> responses; // Long = Question Id, String = Responses

    private long ownerId;

    public CheckInItem() {
        responses = new HashMap<Long,String>();
    }

    public CheckInItem(long responseId, long timestamp, long ownerId) {
        this.responseId = responseId;
        this.timestamp = timestamp;
        this.ownerId = ownerId;
        responses = new HashMap<Long,String>();
    }

    public CheckInItem(long responseId, long timestamp, long ownerId, Map<Long,String> responses) {
        this.responses = responses;
        this.timestamp = timestamp;
        this.ownerId = ownerId;
        this.responseId = responseId;
    }

    public long getResponseId() {
        return responseId;
    }

    public void setResponseId(long responseId) {
        this.responseId = responseId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<Long, String> getResponses() {
        return responses;
    }

    public void setResponses(Map<Long, String> responses) {
        this.responses = responses;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getAnswer(long questionId) {
        String answer = responses.get(questionId);
        if (answer == null) {
            throw new IllegalArgumentException("Question Id cannot be resolved.");
        }
        return answer;
    }

    public void addAnswer(long questionId, String answer) {
        responses.put(questionId, answer);
    }

    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(responseId, timestamp, ownerId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CheckInItem) {
            CheckInItem other = (CheckInItem) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(responseId, other.responseId)
                    && Objects.equal(timestamp, other.timestamp)
                    && Objects.equal(ownerId, other.ownerId);
        } else {
            return false;
        }
    }

    public ContentValues getCheckinCvs()
    {
        ContentValues cvs = new ContentValues();
        cvs.put(CheckInContract.CheckInEntry.COLUMN_OWNER, getOwnerId());
        cvs.put(CheckInContract.CheckInEntry.COLUMN_TIMESTAMP, getTimestamp());
        return cvs;
    }

    public ContentValues[] getAnswersCvs() {
        ArrayList<ContentValues> cvsArray =
                new ArrayList<ContentValues>();

        for (Map.Entry<Long, String> entry : responses.entrySet()) {
            long questionId = entry.getKey();
            String answer = entry.getValue();
            ContentValues cvs = new ContentValues();
            cvs.put(CheckInContract.AnswerEntry.COLUMN_CHECKINID, responseId);
            cvs.put(CheckInContract.AnswerEntry.COLUMN_QUESTIONID, questionId);
            cvs.put(CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT, answer);
            cvsArray.add(cvs);
        }
        return cvsArray.toArray(new ContentValues[cvsArray.size()]);
    }


    public static Collection<CheckInItem> fromCursor(Cursor cursor) {
        ArrayList<CheckInItem> checkInItems = parseCheckinData(cursor);

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndexOrThrow(
                    CheckInContract.AnswerEntry.COLUMN_CHECKINID);
            int idQuestion = cursor.getColumnIndexOrThrow(
                    CheckInContract.AnswerEntry.COLUMN_QUESTIONID);
            int idAnswer = cursor.getColumnIndexOrThrow(
                    CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT);

            while (!cursor.isAfterLast()) {
                long checkInId = cursor.getLong(idIndex);
                long questionId = cursor.getLong(idQuestion);
                String answer = cursor.getString(idAnswer);

                for (CheckInItem item : checkInItems) {
                    if(item.getResponseId() == checkInId) {
                        item.addAnswer(questionId, answer);
                    }
                }
                cursor.moveToNext();
            }
            cursor.close();
        }
        return checkInItems;
    }

    private static ArrayList<CheckInItem> parseCheckinData(Cursor cursor){
        ArrayList<CheckInItem> checkInItems =
                new ArrayList<CheckInItem>();

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndexOrThrow(
                    CheckInContract.AnswerEntry.COLUMN_CHECKINID);
            int idTimestamp = cursor.getColumnIndexOrThrow(
                    CheckInContract.CheckInEntry.COLUMN_TIMESTAMP);
            int idOwner = cursor.getColumnIndexOrThrow(
                    CheckInContract.CheckInEntry.COLUMN_OWNER);

            while (!cursor.isAfterLast()) {
                CheckInItem checkInItem =
                        new CheckInItem(
                                cursor.getLong(idIndex),
                                cursor.getLong(idTimestamp),
                                cursor.getLong(idOwner)
                        );

                if (!checkInItems.contains(checkInItem)){
                    checkInItems.add(checkInItem);
                }
                cursor.moveToNext();
            }
        }
        return checkInItems;
    }
}
