package com.amplioapps.capstonebeta.model.sync;

import retrofit.RequestInterceptor;

public class TokenInterceptor implements RequestInterceptor {

    private String oauthToken;

    public TokenInterceptor(String oauthToken){
        this.oauthToken = oauthToken;
    }

    @Override
    public void intercept(RequestFacade request) {
        request.addHeader("Authorization", "Bearer " + oauthToken);
    }

}
