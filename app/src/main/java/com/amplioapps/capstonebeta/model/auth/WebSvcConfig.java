package com.amplioapps.capstonebeta.model.auth;

public class WebSvcConfig {

    //public static final String TEST_URL = "https://localhost:8443";

    public static final String TEST_URL = "https://10.0.2.2:8443";
    public static final String TOKEN_PATH = "/oauth/token";

    public static final String TOKEN_ENDPOINT = TEST_URL + TOKEN_PATH;
    public static final String CLIENT_ID = "mobile";
    public static final String CLIENT_SECRET = "";

    public static final String TOKEN_TYPE_FULL = "Full access";
    public static final String TOKEN_TYPE_FULL_LABEL = "Full access to an Udinic account";

    public static final String TOKEN_TYPE_READ_ONLY = "Read only";
    public static final String TOKEN_TYPE_READ_ONLY_LABEL = "Read only access to an Udinic account";



    public static final WebSvcAuthentication sWebSvcAuth = new WebSvcAuthentication();
}
