package com.amplioapps.capstonebeta.model;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;


public class GotItProvider extends ContentProvider {

    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG =
            GotItProvider.class.getSimpleName();

    /**
     * Implementation of the HobbitProvider, which is either
     * HobbitProviderHashMap or HobbiProviderSQLite.
     */
    private GotItProviderImpl mImpl;

    /**
     * Method called to handle type requests from client applications.
     * It returns the MIME type of the data associated with each
     * URI.  */
    @Override
    public String getType(Uri uri) {
        return mImpl.getType(uri);
    }

    /**
     * Method called to handle insert requests from client
     * applications.
     */
    @Override
    public Uri insert(Uri uri,
                      ContentValues cvs) {
        return mImpl.insert(uri, cvs);
    }

    /**
     * Method that handles bulk insert requests.
     */
    @Override
    public int bulkInsert(Uri uri,
                          ContentValues[] cvsArray) {
        return mImpl.bulkInsert(uri, cvsArray);
    }

    /**
     * Method called to handle query requests from client
     * applications.
     */
    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        return mImpl.query(uri,
                projection,
                selection,
                selectionArgs,
                sortOrder);
    }

    /**
     * Method called to handle update requests from client
     * applications.
     */
    @Override
    public int update(Uri uri,
                      ContentValues cvs,
                      String selection,
                      String[] selectionArgs) {
        return mImpl.update(uri,
                cvs,
                selection,
                selectionArgs);
    }

    /**
     * Method called to handle delete requests from client
     * applications.
     */
    @Override
    public int delete(Uri uri,
                      String selection,
                      String[] selectionArgs) {
        return mImpl.delete(uri,
                selection,
                selectionArgs);
    }

    /**
     * Return true if successfully started.
     */
    @Override
    public boolean onCreate() {
        mImpl = new GotItProviderImpl(getContext());
        return mImpl.onCreate();
    }
}
