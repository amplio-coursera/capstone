package com.amplioapps.capstonebeta.model.auth;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.FormUrlEncodedTypedOutput;

import com.amplioapps.capstonebeta.model.auth.SignupSvcApi;
import com.amplioapps.capstonebeta.model.auth.WebSvcConfig;
import com.amplioapps.capstonebeta.model.sync.SecuredRestException;
import com.amplioapps.capstonebeta.model.sync.SecuredWebSvcApi;
import com.amplioapps.capstonebeta.model.sync.TokenInterceptor;
import com.amplioapps.capstonebeta.model.sync.UnsafeHttpsClient;
import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class WebSvcAuthentication {

    private final Client client = new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient());
    private final String tokenIssuingEndpoint = WebSvcConfig.TOKEN_ENDPOINT;
    private final String clientId = WebSvcConfig.CLIENT_ID;
    private final String clientSecret = WebSvcConfig.CLIENT_SECRET;
    private String accessToken;

    private final SignupSvcApi accountSvc = new RestAdapter.Builder()
            .setClient(client)
            .setEndpoint(WebSvcConfig.TEST_URL)
            .build().create(SignupSvcApi.class);

    private SignupAccountDetails mUser;

    public String signIn(String username, String password){
        try {
            // This code below programmatically builds an OAuth 2.0 password
            // grant request and sends it to the server.

            // Encode the username and password into the body of the request.
            FormUrlEncodedTypedOutput to = new FormUrlEncodedTypedOutput();
            to.addField("username", username);
            to.addField("password", password);

            // Add the client ID and client secret to the body of the request.
            to.addField("client_id", clientId);
            to.addField("client_secret", clientSecret);

            // Indicate that we're using the OAuth Password Grant Flow
            // by adding grant_type=password to the body
            to.addField("grant_type", "password");

            // The password grant requires BASIC authentication of the client.
            // In order to do BASIC authentication, we need to concatenate the
            // client_id and client_secret values together with a colon and then
            // Base64 encode them. The final value is added to the request as
            // the "Authorization" header and the value is set to "Basic "
            // concatenated with the Base64 client_id:client_secret value described
            // above.
            String base64Auth = BaseEncoding.base64()
                    .encode(new String(clientId + ":" + clientSecret).getBytes());

            // Add the basic authorization header
            List<Header> headers = new ArrayList<Header>();
            headers.add(new Header("Authorization", "Basic " + base64Auth));

            // Create the actual password grant request using the data above
            Request req = new Request("POST", tokenIssuingEndpoint, headers, to);

            // Request the password grant.
            Response resp = client.execute(req);

            // Make sure the server responded with 200 OK
            if (resp.getStatus() < 200 || resp.getStatus() > 299) {
                // If not, we probably have bad credentials
                throw new SecuredRestException("Login failure: "
                        + resp.getStatus() + " - " + resp.getReason());
            } else {
                // Extract the string body from the response
                String body = IOUtils.toString(resp.getBody().in());

                // Extract the access_token (bearer token) from the response so that we
                // can add it to future requests.
                accessToken = new Gson()
                        .fromJson(body, JsonObject.class)
                        .get("access_token")
                        .getAsString();

                getUserAccount(accessToken);

                return accessToken;
            }

        } catch (Exception e) {
            throw new SecuredRestException(e);
        }
    }

    public String signUp(SignupAccountDetails account){
        try {

            mUser = accountSvc.addUserAccount(account);

            if (!account.equals(mUser)){
                throw new SecuredRestException("Signup failure: Account was not created");
            }
            else
                return signIn(account.getEmail(), account.getPassword());

        }catch (Exception e) {
            throw new SecuredRestException(e);
        }
    }

    public long getUserId() {
        return mUser.getId();
    }

    private void getUserAccount(String authToken) {
        TokenInterceptor interceptor = new TokenInterceptor(authToken);

        final SecuredWebSvcApi accountSvc = new RestAdapter.Builder()
                .setClient(client)
                .setEndpoint(WebSvcConfig.TEST_URL)
                .setRequestInterceptor(interceptor) // Register Oauth interceptor
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build().create(SecuredWebSvcApi.class);

        mUser = accountSvc.getUserObject();
    }

}
