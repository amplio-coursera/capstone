package com.amplioapps.capstonebeta.model;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;


public class GotItProviderImpl {

    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG =
            GotItProvider.class.getSimpleName();

    /**
     * Use HobbitDatabaseHelper to manage database creation and version
     * management.
     */
    private GotItDatabaseHelper mOpenHelper;

    /**
     * Context used for various ContentResolver activities.
     */
    protected Context mContext;

    /**
     * Constructor initializes the Context field.
     */
    public GotItProviderImpl(Context context) {
        mContext = context;
    }

    /**
     * The code that is returned when a URI for more than 1 items is
     * matched against the given components.  Must be positive.
     */
    public static final int ANSWERS = 100;
    public static final int QUESTIONS = 200;
    public static final int CHECKINS = 300;
    public static final int USERS = 400;
    public static final int FOLLOWERS = 500;
    public static final int FOLLOWINGS = 600;

    /**
     * The code that is returned when a URI for exactly 1 item is
     * matched against the given components.  Must be positive.
     */
    public static final int ANSWER = 101;
    public static final int QUESTION = 201;
    public static final int CHECKIN = 301;
    public static final int USER = 401;
    public static final int FOLLOWER = 501;
    public static final int FOLLOWING = 601;


    /**
     * URI Matcher code to get the entire Checkin with Answers Item
     */

    public static final int CHECKIN_ANSWER = 1000;

    // Constants for custom queries

    private static final String CHECKIN_TABLE_NAME =
            CheckInContract.CheckInEntry.TABLE_NAME;

    private static final String ANSWER_TABLE_NAME =
            CheckInContract.AnswerEntry.TABLE_NAME;


    public static final int CHECKIN_USER = 2000;

    // Constants for custom queries

    private static final String USER_TABLE_NAME =
            CheckInContract.UsersEntry.TABLE_NAME;


    public static final int FOLLOWER_USER = 3000;

    // Constants for custom queries

    private static final String FOLLOWER_TABLE_NAME =
            CheckInContract.FollowerEntry.TABLE_NAME;


    public static final int FOLLOWING_USER = 4000;

    // Constants for custom queries

    private static final String FOLLOWING_TABLE_NAME =
            CheckInContract.FollowingEntry.TABLE_NAME;

    /**
     * The URI Matcher used by this content provider.
     */
    public static final UriMatcher sUriMatcher =
            buildUriMatcher();

    /**
     * Helper method to match each URI to the ACRONYM integers
     * constant defined above.
     *
     * @return UriMatcher
     */
    protected static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher =
                new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_ANSWER,
                ANSWERS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_ANSWER
                        + "/#",
                ANSWER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_CHECKIN,
                CHECKINS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_CHECKIN
                        + "/#",
                CHECKIN);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_QUESTION,
                QUESTIONS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_QUESTION
                        + "/#",
                QUESTION);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_USERS,
                USERS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_USERS
                        + "/#",
                USER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWER,
                FOLLOWERS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWER
                        + "/#",
                FOLLOWER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWING,
                FOLLOWINGS);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWING
                        + "/#",
                FOLLOWING);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_CHECKIN_ANSWER,
                CHECKIN_ANSWER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_CHECKIN_USER,
                CHECKIN_USER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWER_USER,
                FOLLOWER_USER);
        matcher.addURI(CheckInContract.CONTENT_AUTHORITY,
                CheckInContract.PATH_FOLLOWING_USER,
                FOLLOWING_USER);
        return matcher;
    }

    /**
     * Method called to handle type requests from client applications.
     * It returns the MIME type of the data associated with each URI.
     */
    public String getType(Uri uri) {
        // Match the id returned by UriMatcher to return appropriate
        // MIME_TYPE.
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                return CheckInContract.AnswerEntry.CONTENT_ITEMS_TYPE;
            case ANSWER:
                return CheckInContract.AnswerEntry.CONTENT_ITEM_TYPE;
            case CHECKINS:
                return CheckInContract.CheckInEntry.CONTENT_ITEMS_TYPE;
            case CHECKIN:
                return CheckInContract.CheckInEntry.CONTENT_ITEM_TYPE;
            case QUESTIONS:
                return CheckInContract.QuestionEntry.CONTENT_ITEMS_TYPE;
            case QUESTION:
                return CheckInContract.QuestionEntry.CONTENT_ITEM_TYPE;
            case USERS:
                return CheckInContract.UsersEntry.CONTENT_ITEMS_TYPE;
            case USER:
                return CheckInContract.UsersEntry.CONTENT_ITEM_TYPE;
            case FOLLOWERS:
                return CheckInContract.FollowerEntry.CONTENT_ITEMS_TYPE;
            case FOLLOWER:
                return CheckInContract.FollowerEntry.CONTENT_ITEM_TYPE;
            case FOLLOWINGS:
                return CheckInContract.FollowingEntry.CONTENT_ITEMS_TYPE;
            case FOLLOWING:
                return CheckInContract.FollowingEntry.CONTENT_ITEM_TYPE;
            case CHECKIN_ANSWER:
                return CheckInContract.CHECKIN_ANSWER_TYPE;
            case CHECKIN_USER:
                return CheckInContract.CHECKIN_USER_TYPE;
            case FOLLOWER_USER:
                return CheckInContract.FOLLOWER_USER_TYPE;
            case FOLLOWING_USER:
                return CheckInContract.FOLLOWING_USER_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }
    }

    /**
     * Method called to handle insert requests from client
     * applications.  This method plays the role of the "template
     * method" in the Template Method pattern.
     */
    public Uri insert(Uri uri,
                      ContentValues cvs) {
        Uri returnUri;

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match insert a new
        // row.
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                returnUri = insertAnswers(uri,
                        cvs);
                break;
            case QUESTIONS:
                returnUri = insertQuestions(uri,
                        cvs);
                break;
            case CHECKINS:
                returnUri = insertCheckins(uri,
                        cvs);
                break;
            case USERS:
                returnUri = insertUsers(uri,
                        cvs);
                break;
            case FOLLOWERS:
                returnUri = insertFollowers(uri,
                        cvs);
                break;
            case FOLLOWINGS:
                returnUri = insertFollowings(uri,
                        cvs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        // Notifies registered observers that a row was inserted.
        mContext.getContentResolver().notifyChange(uri,
                null);

        mContext.getContentResolver()
                .notifyChange
                        (CheckInContract.CHECKIN_ANSWER_URI,
                                null);

        mContext.getContentResolver()
                .notifyChange
                        (CheckInContract.CHECKIN_USER_URI,
                                null);

        mContext.getContentResolver()
                .notifyChange
                        (CheckInContract.FOLLOWER_USER_URI,
                                null);

        return returnUri;
    }

    /**
     * Inserts @a ContentValues into the table.  This method plays the
     * role of the "abstract hook method" in the Template Method pattern.
     */

    public Uri insertUsers(Uri uri, ContentValues cvs){
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.UsersEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.UsersEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    public Uri insertFollowers(Uri uri, ContentValues cvs){
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.FollowerEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.FollowerEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    public Uri insertFollowings(Uri uri, ContentValues cvs){
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.FollowingEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.FollowingEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    public Uri insertAnswers(Uri uri, ContentValues cvs){
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.AnswerEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.AnswerEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    public Uri insertQuestions(Uri uri, ContentValues cvs) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.QuestionEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.QuestionEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    public Uri insertCheckins(Uri uri, ContentValues cvs) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        long id =
                db.insert(CheckInContract.CheckInEntry.TABLE_NAME,
                        null,
                        cvs);

        // Check if a new row is inserted or not.
        if (id > 0)
            return CheckInContract.CheckInEntry.buildUri(id);
        else
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
    }

    /**
     * Method that handles bulk insert requests.  This method plays
     * the role of the "template method" in the Template Method
     * pattern.
     */
    public int bulkInsert(Uri uri,
                          ContentValues[] cvsArray) {
        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match insert new
        // rows.

        int returnCount = 0;
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                returnCount = bulkInsertAnswers(uri,
                        cvsArray);

                if (returnCount > 0) {
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_ANSWER_URI,
                                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_USER_URI,
                                            null);
                }
                return returnCount;
            case QUESTIONS:
                returnCount = bulkInsertQuestions(uri,
                        cvsArray);

                if (returnCount > 0)
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);
                return returnCount;
            case CHECKINS:
                returnCount = bulkInsertCheckins(uri,
                        cvsArray);

                if (returnCount > 0) {
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_ANSWER_URI,
                                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_USER_URI,
                                            null);
                }
                return returnCount;
            case USERS:
                returnCount = bulkInsertUsers(uri,
                        cvsArray);

                if (returnCount > 0) {
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);

                    // Register a change to the custom query uri.
                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_ANSWER_URI,
                                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.CHECKIN_USER_URI,
                                            null);
                }
                return returnCount;
            case FOLLOWINGS:
                returnCount = bulkInsertFollowings(uri,
                        cvsArray);

                if (returnCount > 0)
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);

                mContext.getContentResolver()
                        .notifyChange
                                (CheckInContract.FOLLOWING_USER_URI,
                                        null);
                return returnCount;
            case FOLLOWERS:
                returnCount = bulkInsertFollowers(uri,
                        cvsArray);

                if (returnCount > 0) {
                    // Notifies registered observers that row(s) were
                    // inserted.
                    mContext.getContentResolver().notifyChange(uri,
                            null);

                    mContext.getContentResolver()
                            .notifyChange
                                    (CheckInContract.FOLLOWER_USER_URI,
                                            null);
                }
                return returnCount;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /**
     * Inserts an array of @a ContentValues into the table.  This
     * method plays the role of the "abstract hook method" in the
     * Template Method pattern.
     */

    public int bulkInsertUsers(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.UsersEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    public int bulkInsertFollowings(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.FollowingEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    public int bulkInsertFollowers(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.FollowerEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    public int bulkInsertAnswers(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.AnswerEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    public int bulkInsertQuestions(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.QuestionEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    public int bulkInsertCheckins(Uri uri, ContentValues[] cvsArray) {
        final SQLiteDatabase db =
                mOpenHelper.getWritableDatabase();

        int returnCount = 0;

        // Begins a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        try {
            for (ContentValues cvs : cvsArray) {
                final long id =
                        db.insert(CheckInContract.CheckInEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id != -1)
                    returnCount++;
            }

            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End a transaction.
            db.endTransaction();
        }
        return returnCount;
    }

    /**
     * Method called to handle query requests from client
     * applications.  This method plays the role of the "template
     * method" in the Template Method pattern.
     */
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        Cursor cursor;

        // Match the id returned by UriMatcher to query appropriate
        // rows.
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                cursor = queryAnswers(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case ANSWER:
                cursor = queryAnswer(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case QUESTIONS:
                cursor = queryQuestions(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case QUESTION:
                cursor = queryQuestion(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case CHECKINS:
                cursor = queryCheckins(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case CHECKIN:
                cursor = queryCheckin(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case USERS:
                cursor = queryUsers(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case USER:
                cursor = queryUser(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case FOLLOWERS:
                cursor = queryFollowers(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case FOLLOWER:
                cursor = queryFollower(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case FOLLOWINGS:
                cursor = queryFollowings(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case FOLLOWING:
                cursor = queryFollowing(uri,
                        projection,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;
            case CHECKIN_ANSWER:
                if (selectionArgs == null)
                    cursor = queryCheckinAnswer(null,uri);
                else
                    cursor = queryCheckinAnswer(selectionArgs[0],uri);
                break;
            case CHECKIN_USER:
                cursor = queryCheckinUser(selectionArgs[0],uri);
                break;
            case FOLLOWER_USER:
                cursor = queryFollowerUser(selectionArgs[0],uri);
                break;
            case FOLLOWING_USER:
                cursor = queryFollowingUser(selectionArgs[0],uri);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        // Register to watch a content URI for changes.
        cursor.setNotificationUri(mContext.getContentResolver(),
                uri);
        return cursor;
    }

    public Cursor queryFollowingUser(String userid, Uri uri) {

        final String FROM_WHERE_STATEMENT_ALL_FOLLOWING_USER =
                "SELECT "
                        + FOLLOWING_TABLE_NAME
                        + "."
                        + CheckInContract.FollowingEntry._ID
                        + ", "
                        + CheckInContract.FollowingEntry.COLUMN_OWNERID
                        + ", "
                        + CheckInContract.UsersEntry.COLUMN_USERID
                        + ", "
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " FROM "
                        + FOLLOWING_TABLE_NAME
                        + ", "
                        + USER_TABLE_NAME
                        + " WHERE "
                        + FOLLOWING_TABLE_NAME
                        + "."
                        + CheckInContract.FollowingEntry.COLUMN_OWNERID
                        + " = ? AND "
                        + FOLLOWING_TABLE_NAME
                        + "."
                        + CheckInContract.FollowingEntry.COLUMN_FOLLOWINGID
                        + " = "
                        + USER_TABLE_NAME
                        + "."
                        + CheckInContract.UsersEntry.COLUMN_USERID
                        + " ORDER BY "
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " ASC";

        // Retreive the database from the helper
        final SQLiteDatabase db =
                mOpenHelper.getReadableDatabase();

        // Formulate the query statement.
        final String selectQuery =
                FROM_WHERE_STATEMENT_ALL_FOLLOWING_USER;

        Log.v(TAG,
                selectQuery);

        // Query the SQLite database using the all-locations Uri,
        // which returns a Cursor joining the Weather Values and
        // Conditions table entries for one WeatherData object for the
        // target location.
        Cursor result = db.rawQuery(selectQuery,
                new String[] { userid });

        return result;
    }


    public Cursor queryFollowerUser(String userid, Uri uri) {

        final String FROM_WHERE_STATEMENT_ALL_FOLLOWER_USER =
                "SELECT "
                        + FOLLOWER_TABLE_NAME
                        + "."
                        + CheckInContract.FollowerEntry._ID
                        + ", "
                        + CheckInContract.FollowerEntry.COLUMN_OWNERID
                        + ", "
                        + CheckInContract.UsersEntry.COLUMN_USERID
                        + ", "
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " FROM "
                        + FOLLOWER_TABLE_NAME
                        + ", "
                        + USER_TABLE_NAME
                        + " WHERE "
                        + FOLLOWER_TABLE_NAME
                        + "."
                        + CheckInContract.FollowerEntry.COLUMN_OWNERID
                        + " = ? AND "
                        + FOLLOWER_TABLE_NAME
                        + "."
                        + CheckInContract.FollowerEntry.COLUMN_FOLLOWERID
                        + " = "
                        + USER_TABLE_NAME
                        + "."
                        + CheckInContract.UsersEntry.COLUMN_USERID
                        + " ORDER BY "
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " ASC";

        // Retreive the database from the helper
        final SQLiteDatabase db =
                mOpenHelper.getReadableDatabase();

        // Formulate the query statement.
        final String selectQuery =
                FROM_WHERE_STATEMENT_ALL_FOLLOWER_USER;

        Log.v(TAG,
                selectQuery);

        // Query the SQLite database using the all-locations Uri,
        // which returns a Cursor joining the Weather Values and
        // Conditions table entries for one WeatherData object for the
        // target location.
        Cursor result = db.rawQuery(selectQuery,
                new String[] { userid });

        return result;
    }

    public Cursor queryCheckinUser(String userid, Uri uri) {

        final String FROM_WHERE_STATEMENT_ALL_CHECKIN_USER =
                "SELECT "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry._ID
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + ", "
                        + CheckInContract.UsersEntry.COLUMN_USERNAME
                        + " FROM "
                        + CHECKIN_TABLE_NAME
                        + ", "
                        + USER_TABLE_NAME
                        + " WHERE "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + " = ? AND "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + " = "
                        + USER_TABLE_NAME
                        + "."
                        + CheckInContract.UsersEntry.COLUMN_USERID
                        + " ORDER BY "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + " DESC";

        // Retreive the database from the helper
        final SQLiteDatabase db =
                mOpenHelper.getReadableDatabase();

        // Formulate the query statement.
        final String selectQuery =
                FROM_WHERE_STATEMENT_ALL_CHECKIN_USER;

        Log.v(TAG,
                selectQuery);

        // Query the SQLite database using the all-locations Uri,
        // which returns a Cursor joining the Weather Values and
        // Conditions table entries for one WeatherData object for the
        // target location.
        Cursor result = db.rawQuery(selectQuery,
                new String[] { userid });

        return result;
    }


    public Cursor queryCheckinAnswer(String userid, Uri uri) {
        final String FROM_WHERE_STATEMENT_CHECKIN_ANSWER =
                "SELECT "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry._ID
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_CHECKINID
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_QUESTIONID
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT
                        + " FROM "
                        + CHECKIN_TABLE_NAME
                        + ", "
                        + ANSWER_TABLE_NAME
                        + " WHERE "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + " = ? AND "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry._ID
                        + " = "
                        + ANSWER_TABLE_NAME
                        + "."
                        + CheckInContract.AnswerEntry.COLUMN_CHECKINID
                        + " ORDER BY "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + " ASC";

        final String FROM_WHERE_STATEMENT_ALL_CHECKIN_ANSWER =
                "SELECT "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry._ID
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + ", "
                        + CheckInContract.CheckInEntry.COLUMN_OWNER
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_CHECKINID
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_QUESTIONID
                        + ", "
                        + CheckInContract.AnswerEntry.COLUMN_ANSWERTEXT
                        + " FROM "
                        + CHECKIN_TABLE_NAME
                        + ", "
                        + ANSWER_TABLE_NAME
                        + " WHERE "
                        + CHECKIN_TABLE_NAME
                        + "."
                        + CheckInContract.CheckInEntry._ID
                        + " = "
                        + ANSWER_TABLE_NAME
                        + "."
                        + CheckInContract.AnswerEntry.COLUMN_CHECKINID
                        + " ORDER BY "
                        + CheckInContract.CheckInEntry.COLUMN_TIMESTAMP
                        + " ASC";

        // Retreive the database from the helper
        final SQLiteDatabase db =
                mOpenHelper.getReadableDatabase();

        String selectQuery;
        String[] params;

        if (userid == null) {
            selectQuery = FROM_WHERE_STATEMENT_ALL_CHECKIN_ANSWER;
            params = null;
        }
        else {
            selectQuery = FROM_WHERE_STATEMENT_CHECKIN_ANSWER;
            params = new String[] { userid };
        }

        Log.v(TAG,
                selectQuery);

        Cursor result = db.rawQuery(selectQuery, params);

        return result;
    }


    /**
     * Queries for a @a selection in the entire table, relative to
     * the @a selectionArgs.  This method plays the role of the
     * "abstract hook method" in the Template Method pattern.
     */

    public Cursor queryUsers
    (Uri uri,
     String[] projection,
     String selection,
     String[] selectionArgs,
     String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.UsersEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public Cursor queryFollowers
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public Cursor queryFollowings
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public Cursor queryAnswers
    (Uri uri,
     String[] projection,
     String selection,
     String[] selectionArgs,
     String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public Cursor queryQuestions
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public Cursor queryCheckins
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        selection = addSelectionArgs(selection,
                selectionArgs,
                "OR");
        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    /**
     * Queries for a @a selection in a particular row of the table,
     * relative to the @a selectionArgs.  This method plays the role
     * of the "abstract hook method" in the Template Method pattern.
     */

    public  Cursor queryUser
    (Uri uri,
     String[] projection,
     String selection,
     String[] selectionArgs,
     String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.UsersEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.UsersEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public  Cursor queryFollower
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowerEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public  Cursor queryFollowing
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowingEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public  Cursor queryAnswer
    (Uri uri,
     String[] projection,
     String selection,
     String[] selectionArgs,
     String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.AnswerEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public  Cursor queryQuestion
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.QuestionEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    public  Cursor queryCheckin
            (Uri uri,
             String[] projection,
             String selection,
             String[] selectionArgs,
             String sortOrder){

        return mOpenHelper.getReadableDatabase().query
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        projection,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.CheckInEntry.TABLE_NAME),
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
    }

    /**
     * Method called to handle update requests from client
     * applications.  This method plays the role of the "template
     * method" in the Template Method pattern.
     */
    public int update(Uri uri,
                      ContentValues cvs,
                      String selection,
                      String[] selectionArgs) {
        int recsUpdated = 0;

        // Match the id returned by UriMatcher to update appropriate
        // rows.
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                recsUpdated = updateAnswers(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case ANSWER:
                recsUpdated = updateAnswer(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case QUESTIONS:
                recsUpdated = updateQuestions(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case QUESTION:
                recsUpdated = updateQuestion(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case CHECKINS:
                recsUpdated = updateCheckins(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case CHECKIN:
                recsUpdated = updateCheckin(uri,
                        cvs,
                        selection,
                        selectionArgs);
            case USERS:
                recsUpdated = updateUsers(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case USER:
                recsUpdated = updateUser(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWERS:
                recsUpdated = updateFollowers(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWER:
                recsUpdated = updateFollower(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWINGS:
                recsUpdated = updateFollowings(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWING:
                recsUpdated = updateFollowing(uri,
                        cvs,
                        selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        if (recsUpdated > 0) {
            // Notifies registered observers that row(s) were
            // inserted.
            mContext.getContentResolver().notifyChange(uri,
                    null);

            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.CHECKIN_ANSWER_URI,
                                    null);

            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.CHECKIN_USER_URI,
                                    null);

            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.FOLLOWER_USER_URI,
                                    null);

            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.FOLLOWING_USER_URI,
                                    null);
        }
        return recsUpdated;
    }

    /**
     * Update a @a selection in the entire table, relative to the @a
     * selectionArgs.  This method plays the role of the "abstract hook method"
     * in the Template Method pattern.
     */

    public  int updateUsers
    (Uri uri,
     ContentValues cvs,
     String selection,
     String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.UsersEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    public  int updateFollowers
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    public  int updateFollowings
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    public  int updateAnswers
    (Uri uri,
     ContentValues cvs,
     String selection,
     String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    public  int updateQuestions
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs){

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    public  int updateCheckins
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs){

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        cvs,
                        selection,
                        selectionArgs);
    }

    /**
     * Update a @a selection in a particular row of the table,
     * relative to the @a selectionArgs.  This method plays the role
     * of the "abstract hook method" in the Template Method pattern.
     */

    public  int updateUser
    (Uri uri,
     ContentValues cvs,
     String selection,
     String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.UsersEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.UsersEntry.TABLE_NAME),
                        selectionArgs);
    }

    public  int updateFollowing
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowingEntry.TABLE_NAME),
                        selectionArgs);
    }


    public  int updateFollower
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowerEntry.TABLE_NAME),
                        selectionArgs);
    }

    public  int updateAnswer
    (Uri uri,
     ContentValues cvs,
     String selection,
     String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.AnswerEntry.TABLE_NAME),
                        selectionArgs);
    }

    public  int updateQuestion
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.QuestionEntry.TABLE_NAME),
                        selectionArgs);
    }

    public  int updateCheckin
            (Uri uri,
             ContentValues cvs,
             String selection,
             String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.CheckInEntry.TABLE_NAME),
                        selectionArgs);
    }

    /**
     * Method called to handle delete requests from client
     * applications.  This method plays the role of the "template
     * method" in the Template Method pattern.
     */
    public int delete(Uri uri,
                      String selection,
                      String[] selectionArgs) {
        int recsDeleted = 0;

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI) or -1 if
        // there is no matched node.  If a match is found delete the
        // appropriate rows.
        switch (sUriMatcher.match(uri)) {
            case ANSWERS:
                recsDeleted = deleteAnswers(uri,
                        selection,
                        selectionArgs);
                break;
            case ANSWER:
                recsDeleted = deleteAnswer(uri,
                        selection,
                        selectionArgs);
                break;
            case QUESTIONS:
                recsDeleted = deleteQuestions(uri,
                        selection,
                        selectionArgs);
                break;
            case QUESTION:
                recsDeleted = deleteQuestion(uri,
                        selection,
                        selectionArgs);
                break;
            case CHECKINS:
                recsDeleted = deleteCheckins(uri,
                        selection,
                        selectionArgs);
                break;
            case CHECKIN:
                recsDeleted = deleteCheckin(uri,
                        selection,
                        selectionArgs);
                break;
            case USERS:
                recsDeleted = deleteUsers(uri,
                        selection,
                        selectionArgs);
                break;
            case USER:
                recsDeleted = deleteUser(uri,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWINGS:
                recsDeleted = deleteFollowings(uri,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWING:
                recsDeleted = deleteFollowing(uri,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWERS:
                recsDeleted = deleteFollowers(uri,
                        selection,
                        selectionArgs);
                break;
            case FOLLOWER:
                recsDeleted = deleteFollower(uri,
                        selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        // Notifies registered observers that row(s) were deleted.
        if (selection == null
                || recsDeleted != 0) {
            mContext.getContentResolver().notifyChange(uri,
                    null);

            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.FOLLOWER_USER_URI,
                                    null);
            mContext.getContentResolver()
                    .notifyChange
                            (CheckInContract.FOLLOWING_USER_URI,
                                    null);
        }
        return recsDeleted;
    }

    /**
     * Delete a @a selection in the entire table, relative to the @a
     * selectionArgs.  This method plays the role of the "abstract
     * hook method" in the Template Method pattern.
     */

    public  int deleteUsers(Uri uri,
                              String selection,
                              String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.UsersEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }

    public  int deleteFollowers(Uri uri,
                            String selection,
                            String[] selectionArgs) {

        //selection = addSelectionArgs(selection,
        //        selectionArgs,
        //        " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }

    public  int deleteFollowings(Uri uri,
                                String selection,
                                String[] selectionArgs) {

        //selection = addSelectionArgs(selection,
        //        selectionArgs,
        //        " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }

    public  int deleteAnswers(Uri uri,
                              String selection,
                              String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }

    public  int deleteQuestions(Uri uri,
                                String selection,
                                String[] selectionArgs) {

        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }

    public  int deleteCheckins(Uri uri,
                               String selection,
                               String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
    }


    /**
     * Delete a @a selection in a particular row of the table,
     * relative to the @a selectionArgs.  This method plays the role
     * of the "abstract hook method" in the Template Method pattern.
     */

    public int deleteUser(Uri uri,
                            String selection,
                            String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.UsersEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.UsersEntry.TABLE_NAME),
                        selectionArgs);
    }

    public int deleteFollowing(Uri uri,
                          String selection,
                          String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.FollowingEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowingEntry.TABLE_NAME),
                        selectionArgs);
    }

    public int deleteFollower(Uri uri,
                               String selection,
                               String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.FollowerEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.FollowerEntry.TABLE_NAME),
                        selectionArgs);
    }

    public int deleteAnswer(Uri uri,
                            String selection,
                            String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.AnswerEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.AnswerEntry.TABLE_NAME),
                        selectionArgs);
    }

    public int deleteQuestion(Uri uri,
                              String selection,
                              String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.QuestionEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.QuestionEntry.TABLE_NAME),
                        selectionArgs);
    }

    public int deleteCheckin(Uri uri,
                             String selection,
                             String[] selectionArgs) {
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just delete a single row in the database.
        return mOpenHelper.getWritableDatabase().delete
                (CheckInContract.CheckInEntry.TABLE_NAME,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri),
                                CheckInContract.CheckInEntry.TABLE_NAME),
                        selectionArgs);
    }


    /**
     * Return true if successfully started.
     */
    public boolean onCreate() {
        // Create the HobbitDatabaseHelper.
        mOpenHelper =
                new GotItDatabaseHelper(mContext);
        return true;
    }

    /**
     * Return a selection string that concatenates all the
     * @a selectionArgs for a given @a selection using the given @a
     * operation.
     */
    private String addSelectionArgs(String selection,
                                    String [] selectionArgs,
                                    String operation) {
        // Handle the "null" case.
        if (selection == null
                || selectionArgs == null)
            return null;
        else {
            String selectionResult = "";

            // Properly add the selection args to the selectionResult.
            for (int i = 0;
                 i < selectionArgs.length - 1;
                 ++i)
                selectionResult += (selection
                        + " = ? "
                        + operation
                        + " ");

            // Handle the final selection case.
            selectionResult += (selection
                    + " = ?");

            // Output the selectionResults to Logcat.
            Log.d(TAG,
                    "selection = "
                            + selectionResult
                            + " selectionArgs = ");
            for (String args : selectionArgs)
                Log.d(TAG,
                        args
                                + " ");

            return selectionResult;
        }
    }

    /**
     * Helper method that appends a given key id to the end of the
     * WHERE statement parameter.
     */
    private static String addKeyIdCheckToWhereStatement(String whereStatement,
                                                        long id,
                                                        String tableName) {
        String tableId = "";
        String newWhereStatement;
        if (TextUtils.isEmpty(whereStatement))
            newWhereStatement = "";
        else
            newWhereStatement = whereStatement + " AND ";

        switch (tableName) {
            case CheckInContract.AnswerEntry.TABLE_NAME:
                tableId = CheckInContract.AnswerEntry._ID;
                break;
            case CheckInContract.QuestionEntry.TABLE_NAME:
                tableId = CheckInContract.QuestionEntry._ID;
                break;
            case CheckInContract.CheckInEntry.TABLE_NAME:
                tableId = CheckInContract.CheckInEntry._ID;
                break;
            case CheckInContract.UsersEntry.TABLE_NAME:
                tableId = CheckInContract.UsersEntry._ID;
                break;
            case CheckInContract.FollowerEntry.TABLE_NAME:
                tableId = CheckInContract.FollowerEntry._ID;
                break;
            case CheckInContract.FollowingEntry.TABLE_NAME:
                tableId = CheckInContract.FollowingEntry._ID;
                break;
        }

        // Append the key id to the end of the WHERE statement.
        return newWhereStatement
                + tableId
                + " = '"
                + id
                + "'";
    }


}