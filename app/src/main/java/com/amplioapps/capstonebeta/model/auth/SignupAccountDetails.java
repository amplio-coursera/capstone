package com.amplioapps.capstonebeta.model.auth;


import com.google.common.base.Objects;

public class SignupAccountDetails {

    private long id;
    private String firstName; // Required
    private String lastName; // Required
    private String email; // Required
    private String password; // Required
    private char gender;
    private long birthDate;
    private String medicalRecord;


    public SignupAccountDetails() {
    }

    public SignupAccountDetails(String firstName, String lastName,
                                String email, String password, String medicalRecord,
                                long birthDate){
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.medicalRecord = medicalRecord;
        this.birthDate = birthDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(firstName);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SignupAccountDetails) {
            SignupAccountDetails other = (SignupAccountDetails) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(firstName, other.firstName)
                    && Objects.equal(lastName, other.lastName)
                    && Objects.equal(email, other.email)
                    && Objects.equal(gender, other.gender)
                    && Objects.equal(birthDate, other.birthDate);

        } else {
            return false;
        }
    }

}
